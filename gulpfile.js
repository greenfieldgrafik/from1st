"use strict";

// Load modules

var $ = require("gulp-load-plugins")();
var browserSync = require("browser-sync");
var cssAutoprefixer = require("autoprefixer");
var cssMqpacker = require("css-mqpacker");
var del = require("del");
var fs = require('fs');
var gulp = require("gulp");
var imgMozjpeg = require("imagemin-mozjpeg");
var imgPngquant = require("imagemin-pngquant");
var imgSvgo = require("imagemin-svgo");
var jshint = require("jshint");
var runSequence = require("run-sequence");
var sass = require("gulp-sass"); // gulp-load-pluginsがsassを読み込まないため


// get Local IP

var os = require("os");
var ifaces = os.networkInterfaces();
var ipAddress;

Object.keys(ifaces).forEach(function (ifname) {
    ifaces[ifname].forEach(function (iface) {
        if ('IPv4' !== iface.family || iface.internal !== false ) {
            return;
        }
        // console.log(ifname, iface.address);
        if (ifname == "en0") {
            ipAddress = iface.address;
        }
    });
});


// Load settings

var pkg = require("./_src/project.json");
var pkgname = pkg.name

var settings = require("./settings.json");
var src = settings.dir.src;
var wordpress = settings.wordpress.use;

var bowerjs = settings.bowerfiles.js;
var bowercss = settings.bowerfiles.css;
var bowerdir = "/_bower_components";

var pagesDir = src + "/pages";

if (wordpress) {
  var build = settings.dir.wpbuild + pkgname;
  var demoFiles = settings.sftpdemo.filesWp;
  var deployFiles = settings.sftpdeploy.filesWp;
} else {
  var build = settings.dir.build;
  var demoFiles = settings.sftpdemo.files;
  var deployFiles = settings.sftpdeploy.files;
}

var demoOpts = settings.sftpdemo.ftpOpts;
var deployOpts = settings.sftpdeploy.ftpOpts;

// Plugin options

var autoprefixerOpts = {
    browsers: ['last 2 versions', 'ios_saf >= 8'],
    cascade: false
};

if (wordpress) {
    var browserSyncOpts = {
        notify: false,
        open: "external",
        proxy: "http://from1stofficial.lo/",
        reloadOnRestart: true
    };
} else {
    var browserSyncOpts = {
        notify: false,
        open: "external",
        server: build,
        reloadOnRestart: true
    };
}

var faviconOpts = {
    appName: pkgname,
    background: "transparent",
    icons: {
        android: false,
        appleIcon: true,
        appleStartup: false,
        coast: false,
        favicons: true,
        firefox: false,
        windows: true,
        yandex: false
    },
    replace: true,
};

var imageminOpts = {
    progressive: true
};

var sassOpts = {
    outputStyle: 'compressed'
};

var uglifyOpts = {
    // preserveComments: "license"
};


// The default task

gulp.task("default", function (callback) {
    runSequence(
        "clean",
        "build",
        callback
    );
});


// Clean task

gulp.task("clean", function () {
    del.sync([
        build + "/*",
        "!" + build + "/.git/**"
    ], {
        dot: true,
    });
});


// Build tasks

gulp.task("build", function (callback) {
    runSequence([
        "build:as-is",
        "build:images",
        "build:pages",
        "build:scripts",
        "build:styles"
    ],
    callback);
});

gulp.task("build:as-is", function (callback) {
    return runSequence([
        "build:as-is:bower-js",
        "build:as-is:bower-css",
        "build:as-is:doc",
        "build:as-is:fonts",
        "build:as-is:icon",
        "build:as-is:wp"
    ],
    callback);
});

    gulp.task("build:as-is:bower-js", function () {
        return gulp.src(bowerjs)
        .pipe($.changed(build + "/assets/js"))
        .pipe(gulp.dest(build + "/assets/js"))
    });

    gulp.task("build:as-is:bower-css", function () {
        return gulp.src(bowercss)
        .pipe($.changed(build + "/assets/css"))
        .pipe(gulp.dest(build + "/assets/css"))
    });

    gulp.task("build:as-is:doc", function () {
        return gulp.src([
            src + "/doc/**/*"
        ], {
            dot: false
        })
        .pipe($.changed(build + "/assets/doc"))
        .pipe(gulp.dest(build + "/assets/doc"))
    });

    gulp.task("build:as-is:fonts", function () {
        return gulp.src([
            src + "/fonts/**/*"
        ], {
            dot: false
        })
        .pipe($.changed(build + "/assets/fonts"))
        .pipe(gulp.dest(build + "/assets/fonts"))
    });

    gulp.task("build:as-is:icon", function () {
        return gulp.src([
            src + "/icon/*"
        ], {
            dot: false
        })
        .pipe($.changed(build + "/assets/icon"))
        .pipe(gulp.dest(build + "/assets/icon"))
    });

    gulp.task("build:as-is:wp", function () {
        return gulp.src([
            src + "/pages/assets/*"
        ], {
            dot: false
        })
        .pipe($.if(wordpress,
            $.changed(build)
        ))
        .pipe($.if(wordpress,
            gulp.dest(build)
        ))
    });

gulp.task("build:images", function (callback) {
  return runSequence(
    "build:images:minify",
    "build:images:favicon",
    callback
  );
});

    gulp.task("build:images:favicon", function () {
        return gulp.src(src + "/images/favicon-master.png")
        .pipe($.favicons(faviconOpts))
        .pipe(gulp.dest(build + "/assets/images/favicon"))
    });

    gulp.task("build:images:minify", function () {
        return gulp.src([
            src + "/images/**/*.@(gif|jpg|png|svg)",
            "!" + src + "/images/favicon-master.png"
        ])
        .pipe($.changed(build + "/assets/images"))
        .pipe($.imagemin([imgMozjpeg(), imgPngquant(), imgSvgo()]))
        .pipe(gulp.dest(build + "/assets/images"))
    });

gulp.task("build:pages", function (callback) {
    return runSequence([
        "build:pages:pug"
    ], callback
    );
});

    gulp.task("build:pages:pug", function () {
        var project = JSON.parse(fs.readFileSync(src + "/project.json", { encoding: "utf8"}));
        return gulp.src(pagesDir + "/**/!(_)*.pug")
        .pipe($.plumber({
            errorHandler: $.notify.onError("Error: <%= error.message %>")
        }))
        .pipe($.pug({
            pretty: true,
            locals: project
        }))
        .pipe($.if(wordpress,
            $.rename({
                extname: ".php"
            })
        ))
        .pipe(gulp.dest(build))
    });

    gulp.task("build:pages:pug-changed", function () {
        var project = JSON.parse(fs.readFileSync(src + "/project.json", { encoding: "utf8"}));
        return gulp.src(pagesDir + "/**/!(_)*.pug")
        .pipe($.plumber({
            errorHandler: $.notify.onError("Error: <%= error.message %>")
        }))
        .pipe($.if(wordpress,
            $.changed(build, {extension: '.php'}),
            $.changed(build, {extension: '.html'})
        ))
        .pipe($.pug({
            pretty: true,
            locals: project
        }))
        .pipe($.if(wordpress,
            $.rename({
                extname: ".php"
            })
        ))
        .pipe(gulp.dest(build))
    });

gulp.task("build:scripts", function (callback) {
    return runSequence(
        "build:scripts:concat",
        "build:scripts:uglify",
        "build:scripts:jshint",
        callback
    );
});

    gulp.task("build:scripts:concat", function () {
        return gulp.src([
            src + "/js/lib/*.js",
            src + "/js/*.js"
        ])
        .pipe($.plumber({
            errorHandler: $.notify.onError("Error: <%= error.message %>")
        }))
        .pipe($.concat('main.js'))
        .pipe(gulp.dest(build + "/assets/js"))
    });

    gulp.task("build:scripts:uglify", function () {
        return gulp.src(build + "/assets/js/main.js")
        .pipe($.plumber({
            errorHandler: $.notify.onError("Error: <%= error.message %>")
        }))
        .pipe($.uglify(uglifyOpts))
        .pipe($.rename({
            extname: ".min.js"
        }))
        .pipe(gulp.dest(build + "/assets/js"))
    });

    gulp.task("build:scripts:jshint", function () {
        return gulp.src(build + "/assets/js/main.js")
        .pipe($.plumber({
            errorHandler: $.notify.onError("Error: <%= error.message %>")
        }))
        .pipe($.jshint())
        .pipe($.jshint.reporter('default'));
    });

gulp.task("build:styles", function () {
    return gulp.src(src + "/sass/*.sass")
    .pipe($.plumber({
        errorHandler: $.notify.onError("Error: <%= error.message %>")
    }))
    .pipe($.sassGlob())
    .pipe(sass(sassOpts))
    .pipe($.postcss([cssAutoprefixer(autoprefixerOpts), cssMqpacker()]))
    .pipe($.rename({
        basename: "main"
    }))
    .pipe(gulp.dest(build + "/assets/css"))
});


// Serve and watch task

gulp.task("watch", function () {
    browserSync(browserSyncOpts);

    gulp.watch([
        src + "/fonts/*",
        src + "/icon/*",
        src + bowerdir + "/*",
        src + "/pages/assets/*"
    ], [
        "build:as-is",
        browserSync.reload
    ]);

    gulp.watch(src + "/images/**/*.@(gif|jpg|png|svg)", [
        "build:images",
        browserSync.reload
    ]);

    gulp.watch([
        pagesDir + "/**/*.pug",
        pagesDir + "/**/*.php",
        "!" + pagesDir + "/templates/**/*.pug",
        "!" + pagesDir + "/templates/**/*.php"
    ], [
        "build:pages:pug-changed",
        browserSync.reload
    ]);

    gulp.watch([
        pagesDir + "/templates/**/*.pug",
        pagesDir + "/templates/**/*.php",
        src + "/project.json"
    ], [
        "build:pages:pug",
        browserSync.reload
    ]);

    gulp.watch(src + "/js/*.js", [
        "build:scripts",
        browserSync.reload
    ]);

    gulp.watch(src + "/sass/**/*.sass", [
        "build:styles",
        browserSync.reload
    ]);
});


// Deploy task
gulp.task("sftp:demo", function () {
    return gulp.src(demoFiles)
    .pipe($.sftp(demoOpts));
});

gulp.task("sftp:deploy", function () {
    return gulp.src(deployFiles)
    .pipe($.sftp(deployOpts));
});
