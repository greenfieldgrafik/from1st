$(function(){
    var
    $body = $('body'),
    $container = $('#container'),
    $header = $container.find('.header'),
    $globalNav = $header.find('.global-nav'),
    $content = $container.find('.main'),
    $footer = $container.find('.footer'),
    $hamburger = $container.find('.hamburger'),
    $pagetop = $container.find('.pagetop, .js-scroll').find("a[href^=#]"),
    $loading = $container.find('.loading'),
    hamburgerState = false,
    scrollPosition;

    document.ondragstart = function(){return false;};

    $(window).on('load', function(){
        // ローディング
        // $loading.fadeOut();

        // スムーズスクロール
        $pagetop.on('click', function() {
            var speed = 700,
                href = $(this).attr("href"),
                target = $(href === "#" || href === "" ? 'html' : href),
                position = target.offset().top,
                winTop = $(window).scrollTop(),
                targetBody;

                $(window).scrollTop( winTop + 1 );
            if ( $('html').scrollTop() > 0 ) {
                targetBody = $('html');
            } else if ( $('body').scrollTop() > 0 ) {
                targetBody = $('body');
            }
            targetBody.animate({scrollTop: position}, speed, 'swing');
            return false;
        });
    });

    // ハンバーガーメニュー　スクロールに対応
    $hamburger.on('click', function(){
        if(hamburgerState == false) {
            scrollPosition = $(window).scrollTop();
            $body.addClass('fixed').css({'top': -scrollPosition});
            $(this).addClass('-open');
            $globalNav.addClass('-open');
            hamburgerState = true;
        } else {
            $body.removeClass('fixed').css({'top': 0});
            $(this).removeClass('-open');
            $globalNav.removeClass('-open');
            window.scrollTo( 0 , scrollPosition );
            hamburgerState = false;
        }
    });

    // ローディング
    setTimeout(function() {
        $('.loading').fadeOut();
    }, 1500);

    function stopload(){
        $('.loading').fadeOut();
    }

    if(navigator.userAgent.match(/(iPhone|iPod|Android)/)){
        $('.global-nav .-wrap .-list .-more').on('click', function(){
            $('.global-nav .-wrap .-list .-link.official').slideToggle();
        });

        $('.global-nav .-wrap .-list .-logo.labo').on('click', function(){
            $('.global-nav .-wrap .-list .-link.labo').slideToggle();
        });

        $('.global-nav .-wrap .-list .-logo.gallery').on('click', function(){
            $('.global-nav .-wrap .-list .-link.gallery').slideToggle();
        });
    }

});
