//<![CDATA[
$.fn.gmap = function(options) {
    //デフォルト値設定
    var defaults = {
            zoom: 15,
            mapTypeId : 'ROADMAP',
            scaleControl: true,
            mapTypeControl: false,
            lat : null,
            lng : null,
            title : '0',
            address : '0',
            infoW: false,
            icon: '/images/common/mapicon.png',
            saturation: -100
    };
    //オプションの初期値設定
    var setting = $.extend({}, defaults, options);

    var $window = $(window),
        mapdiv = $(this).get(0);

    var myOptions = {
        zoom: setting.zoom,
        center: new google.maps.LatLng(setting.lat,setting.lng),
        scaleControl: setting.scaleControl,
        mapTypeControl: setting.mapTypeControl,
        scrollwheel: false
    };

    var map = new google.maps.Map(mapdiv, myOptions);

    map.setMapTypeId(setting.mapTypeId.toLowerCase());

    //スタイル設定
    var stylez = [
      {
        "stylers": [
          { "saturation": setting.saturation }
        ]
      },{
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [/*
          { "color": "#ffffff" },
          { "visibility": "simplified" }
        */]
      },{
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [/*
          { "visibility": "simplified" },
          { "color": "#ffffff" }
        */]
      },{
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [/*
          { "visibility": "simplified" },
          { "color": "#ffffff" }
        */]
      },{
        "featureType": "road",
        "elementType": "labels",
        "stylers": [/*
          { "visibility": "simplified" }
        */]
      },{
        "featureType": "road",
        "elementType": "geometry.fill",
        "stylers": [/*
          { "visibility": "simplified" },
          { "weight": 6 }
        */]
      }
    ];
    var styledMapOptions = {};
    var setMapType =  new google.maps.StyledMapType(stylez,styledMapOptions);
    map.mapTypes.set('simple', setMapType);
    map.setMapTypeId('simple');

    var marker = new google.maps.Marker({
        position: myOptions.center,
        map: map,
        icon: setting.icon
    });

    $window.on("resize",function(){
        google.maps.event.trigger(map, 'resize');
        map.setCenter(myOptions.center);
    });

    var contentString = '<div id="info"><h1 class="gtext">'+setting.title+'</h1>'+'<p class="gtext">'+setting.address+'</p></div>';
    var infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    if(setting.infoW){
        google.maps.event.addListener(marker, 'click', function() {
            infowindow.open(map,marker);
        });
    }

    return this;

};
//]]>
