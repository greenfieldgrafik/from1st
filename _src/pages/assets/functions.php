<?php
//-----------------------------------------------------
// ■管理画面表示時使用関数
//-----------------------------------------------------

// ▼アップデート通知を非表示
add_filter( 'pre_site_transient_update_core', '__return_zero' );

// ▼ログイン画面の画像変更
function custom_login_logo() {
    echo '<style type="text/css">h1 a { width: 200px !important; height: 200px !important; background: url('.home_url('cms/img/logo.png').') no-repeat !important; }</style>';
}
add_action('login_head', 'custom_login_logo');

// ▼ダッシュボードウィジェットの項目非表示
function remove_dashboard_widgets() {
    if (!current_user_can('level_10')) { //level10以下のユーザーの場合ウィジェットをunsetする
        global $wp_meta_boxes;
        unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);        // 現在の状況
        unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);  // 最近のコメント
        unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);   // 被リンク
        unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);          // プラグイン
        unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']);      // 最近の下書き
        unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);            // WordPressブログ
        unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);          // WordPressフォーラム
    }
 }
add_action('wp_dashboard_setup', 'remove_dashboard_widgets');

// ▼管理上部メニューバーの項目の非表示
function setting_mymenu( $wp_admin_bar ) {
    $wp_admin_bar->remove_node('updates');      // アップデート通知
    $wp_admin_bar->remove_node('wp-logo');      // Wpロゴ
    $wp_admin_bar->remove_node('comments');     // コメント
    $wp_admin_bar->remove_node('new-content');  // 新規投稿ボタン
    /* 管理バー右の部分 */
    $wp_admin_bar->remove_node('edit-profile'); // プロフィール編集
    $wp_admin_bar->remove_node('user-info');    // ユーザー
}
add_action( 'admin_bar_menu', 'setting_mymenu', 1000 );

// ▼左サイドメニュー項目削除・変更
function setting_sidemenus () {

    if (!current_user_can('level_10')) { //level10以下のユーザーの場合
        global $menu;
        global $submenu;

        //unset($menu[2]); // ダッシュボード
        //unset($menu[4]); // メニューの線1
        //unset($menu[5]); // 投稿
        //unset($menu[10]); // メディア
        unset($menu[15]); // リンク
        // unset($menu[20]); // ページ
        unset($menu[25]); // コメント
        //unset($menu[59]); // メニューの線2
        unset($menu[60]); // テーマ
        unset($menu[65]); // プラグイン
        unset($menu[70]); // プロフィール
        unset($menu[75]); // ツール
        unset($menu[80]); // 設定
        unset($menu[90]); // メニューの線3

        $menu[5][0] = '新着情報一覧・登録';
        $menu[10][0] = '画像ファイル整理';
        $menu[20][0] = 'ページ編集';

        $submenu['edit.php'][5][0] = '新着情報一覧';

        // サブメニュー非表示
        remove_submenu_page('edit.php', 'edit-tags.php?taxonomy=category'); // 投稿・カテゴリ
        remove_submenu_page('edit.php', 'edit-tags.php?taxonomy=post_tag'); // 投稿・タグ

        // タクソノミー設定非表示
        remove_submenu_page('edit.php?post_type=article', 'edit-tags.php?taxonomy=article-kind&amp;post_type=article');
        remove_submenu_page('edit.php?post_type=architect', 'edit-tags.php?taxonomy=architect-type&amp;post_type=architect');
        remove_submenu_page('edit.php?post_type=ivent', 'edit-tags.php?taxonomy=ivent-type&amp;post_type=ivent');

        // プラグインメニュー非表示
        /*
        echo '<style type="text/css">
            <!--
                #toplevel_page_wpcf7 {display:none;}
                #toplevel_page_ps-taxonomy-expander {display:none;}
            -->
        </style>';
        */
        remove_menu_page('wpcf7');
        remove_menu_page('admin.php?page=ps-taxonomy-expander.php');
    }
}
add_action('admin_menu', 'setting_sidemenus');

// ▼フッター「WordPressリンク」を変更
function setting_footer () {
    echo '';
}
add_filter('admin_footer_text', 'setting_footer');

// ▼クイック編集非表示
function hide_inline_edit_link() {
    echo '<style type="text/css">
        <!--
            span.inline {
                display: none;
            }
        -->
    </style>';
}
add_action( 'admin_print_styles-edit.php', 'hide_inline_edit_link' );


//-----------------------------------------------------
// ■ダッシュボード表示設定
//-----------------------------------------------------
function custom_dashboard_widgets() {
    global $wp_meta_boxes;
    wp_add_dashboard_widget('article_widget', '<span class="color1">物件登録状況</span>', 'dashboard_article_count');
    wp_add_dashboard_widget('architect_widget', '<span class="color1">建築実績企業登録状況</span>', 'dashboard_architect_count');
    wp_add_dashboard_widget('ivent_widget', '<span class="color1">イベント登録状況</span>', 'dashboard_ivent_count');
    wp_add_dashboard_widget('news_widget', '<span class="color1">新着情報表示</span>', 'dashboard_news_count');
    wp_add_dashboard_widget('update_widget', '<span class="color1">各種情報更新</span>', 'dashboard_update');

    echo '<style type="text/css">
        <!--
            .color1 {color: #2126AC;}
            .color2 {color: #6485D1;}
            .color3 {font-size: 1.2em; color: #FF4B4B;}
            .w_button {clear:both; text-align:right; padding-top: 10px;}

            .article_count li {
                width: 50%;
                float: left;
                margin-bottom: 10px;
            }
        -->
    </style>';
}
add_action('wp_dashboard_setup', 'custom_dashboard_widgets');


// ▼ダッシュボードウィジェット(各種情報更新)
function dashboard_update() {
    $strHtml = '';
    $strHtml .= '<p class="color2">各種情報の更新は以下リンクより行って下さい。</p>';
    $strHtml .= '<p>';
    // $strHtml .= '<a href="'.home_url('cms/wp-admin/post.php?post=34&action=edit').'" class="button button-primary">販売実績を更新</a>　';
    $strHtml .= '<a href="'.home_url('cms/wp-admin/post.php?post=121&action=edit').'" class="button button-primary">リンク情報を更新</a>';
    $strHtml .= '　<a href="'.home_url('cms/wp-admin/post.php?post=69&action=edit').'" class="button button-primary">定休日、住宅相談会を設定</a>';
    $strHtml .= '</p>';
    echo $strHtml;
}

// ▼ダッシュボードウィジェット(物件登録状況)
function dashboard_article_count() {

    // wp-config
    global $glo_aryState;

    $aryState = array();
    $strHtml = '';
    $strCount = 0;

    // エリアでループ
    foreach ($glo_aryState as $key => $value) {

        // タクソノミー内の記事一覧を取得
        $args = array(
            'numberposts' => -1,
            'post_type' => 'article',
            'meta_key' => 'state',
            'meta_value' => $value
        );

        global $post;
        $posts = get_posts($args);

        if($posts) {
            // 状態名をキーにした配列に件数を格納
            $aryState[$value] = count($posts);
        }

        wp_reset_query();
    }

    // 登録総数を取得
    $args = array(
        'numberposts' => -1,
        'post_type' => 'article',
    );

    global $post;
    $posts = get_posts($args);
    $strCount = count($posts);

    wp_reset_query();

    if (count($aryState) != 0 ) {
        $strHtml .= '<div class="main">';
        $strHtml .= '<p class="color2">現在の分譲地登録状況を表示しています。</p>';
        $strHtml .= '登録総数　<b class="color3">'.$strCount.'件</b>';
        $strHtml .= '<ul class="article_count">';
        foreach($aryState as $key => $value){
            $strHtml .= '<li class="post-count"><span class="dashicons dashicons-admin-home"></span> '.$key.'　<b class="color3">'.$value.' </b>件 登録済</li>';
        }
        $strHtml .= '</ul>';
        $strHtml .= '<div class="w_button"><a href="'.home_url('/cms/wp-admin/edit.php?post_type=article').'" class="button button-primary">分譲地一覧を表示</a></div>';
        $strHtml .= '</div>';
    }
    echo $strHtml;
}

// ▼ダッシュボードウィジェット(建築事務所登録状況)
function dashboard_architect_count() {

    $strHtml = '';
    $aryCategory = array();

    // カテゴリの登録状況を取得
    $args1 = array(
        'show_count' => 1,
        'echo' => 0,
        'taxonomy' => 'architect-type'
    );
    $aryCategory = get_categories($args1);

    if (count($aryCategory) > 0) {
        $strHtml .= '<div class="main">';
        $strHtml .= '<p class="color2">現在の建築実績企業登録状況を表示しています。</p>';
        $strHtml .= '<ul class="article_count">';

        foreach($aryCategory as $key => $value){
            $strHtml .= '<li class="post-count"><span class="dashicons dashicons-hammer"></span> '.$value->name.'　<b class="color3">'.$value->count.' </b>件 登録済</li>';
        }
        $strHtml .= '</ul>';
        $strHtml .= '<div class="w_button"><a href="'.home_url('/cms/wp-admin/edit.php?post_type=architect').'" class="button button-primary">建築実績企業一覧を表示</a></div>';
        $strHtml .= '</div>';
    }
    echo $strHtml;
}

// ▼ダッシュボードウィジェット(イベント登録状況)
function dashboard_ivent_count() {

    $strHtml = '';
    $aryCategory = array();

    // カテゴリの登録状況を取得
    $args1 = array(
        'show_count' => 1,
        'echo' => 0,
        'taxonomy' => 'ivent-type'
    );
    $aryCategory = get_categories($args1);

    if (count($aryCategory) > 0) {
        $strHtml .= '<div class="main">';
        $strHtml .= '<p class="color2">現在のイベント情報の登録状況を表示しています。</p>';
        $strHtml .= '<ul class="article_count">';

        foreach($aryCategory as $key => $value){
            $strHtml .= '<li class="post-count"><span class="dashicons dashicons-flag"></span> '.$value->name.'　<b class="color3">'.$value->count.' </b>件 登録済</li>';
        }
        $strHtml .= '</ul>';
        $strHtml .= '<div class="w_button"><a href="'.home_url('/cms/wp-admin/edit.php?post_type=ivent').'" class="button button-primary">イベント情報一覧を表示</a></div>';
        $strHtml .= '</div>';
    }
    echo $strHtml;
}

// ▼ダッシュボードウィジェット(新着情報登録状況)
function dashboard_news_count() {
    $strHtml = '';
    $aryCategory = array();

    $aryCategory = get_categories();

    if (count($aryCategory) > 0) {
        $strHtml .= '<div class="main">';
        $strHtml .= '<p class="color2">現在の新着情報登録数を表示しています。</p>';
        $strHtml .= '<ul class="article_count">';

        foreach($aryCategory as $key => $value){
            $strHtml .= '<li class="post-count"><span class="dashicons dashicons-admin-home"></span> '.$value->name.'　<b class="color3">'.$value->count.' </b>件 登録済</li>';
        }
        $strHtml .= '</ul>';
        $strHtml .= '<div class="w_button"><a href="'.home_url('cms/wp-admin/edit.php').'" class="button button-primary">新着情報一覧を表示</a></div>';
        $strHtml .= '</div>';
    }
    echo $strHtml;
}
add_action('wp_dashboard_setup', 'custom_dashboard_widgets');



//-----------------------------------------------------
// ■投稿登録時使用関数
//-----------------------------------------------------

// ▼カテゴリ選択をラジオボタンに
if(strstr($_SERVER['REQUEST_URI'], 'wp-admin/post.php')) {
    ob_start('one_category_only');
}
if(strstr($_SERVER['REQUEST_URI'], 'wp-admin/post-new.php')) {
    ob_start('one_category_only');
}
function one_category_only($content) {
    $content = str_replace('type="checkbox" name="post_category', 'type="radio" name="post_category', $content);
    return $content;
}

// ▼カテゴリ新規追加消去
function hide_category_tabs_adder() {
    global $pagenow;
    global $post_type;
    if (is_admin() && ($pagenow=='post-new.php' || $pagenow=='post.php' || $post_type=='article') ){
        echo '<style type="text/css">
        #category-tabs, #category-adder, #genre-tabs {display:none;}
        #xxx-tabs, #xxx-adder {display:none;}

        .categorydiv .tabs-panel {padding: 0 !important; background: none; border: none !important;}
        </style>';
    }

    echo '<style type="text/css">
        <!--
            #article-kind-tabs {display:none;}
            #architect-type-tabs {display:none;}
            #ivent-type-tabs {display:none;}
        -->
    </style>';

}
add_action( 'admin_head', 'hide_category_tabs_adder' );





//-----------------------------------------------------
// ■投稿一覧表示時使用関数
//-----------------------------------------------------

// ▼記事表示数の変更
function my_edit_posts_per_page($posts_per_page) {
    return 60;
}
add_filter('edit_posts_per_page', 'my_edit_posts_per_page');


// ▼投稿一覧から項目を除外
function custom_columns($columns) {
    unset($columns['tags']);
    unset($columns['comments']);
    return $columns;
}
add_filter( 'manage_posts_columns', 'custom_columns' );


// ▼物件一覧項目変更
function add_columns_article ($columns) {
    // サムネイル用のスタイル
    echo '<style>.column-thumb{width:80px;} #title{width:300px;}</style>';

    // 項目追加
    $columns = array_reverse( $columns, true );
    $columns['state'] = '状態';
    $columns['thumb'] = '<div class="dashicons dashicons-format-image"></div>';
    $columns = array_reverse( $columns, true );
    $columns['modified'] = '最終更新日';
    $columns['title'] = '物件名称';

    return $columns;
}
add_filter( 'manage_edit-article_columns', 'add_columns_article' );


// ▼建築事務所一覧項目変更
function add_columns_architect ($columns) {
    // サムネイル用のスタイル
    echo '<style>.column-thumb{width:80px;} #title{width:300px;}</style>';

    // 項目追加
    $columns['modified'] = '最終更新日';
    $columns['title'] = '名称';

    return $columns;
}
add_filter( 'manage_edit-architect_columns', 'add_columns_architect' );

// ▼イベント一覧項目変更
function add_columns_ivent ($columns) {
    // サムネイル用のスタイル
    echo '<style>.column-thumb{width:80px;} #title{width:300px;}</style>';

    // 項目追加
    $columns = array_reverse( $columns, true );
    $columns['thumb'] = '<div class="dashicons dashicons-format-image"></div>';
    $columns = array_reverse( $columns, true );
    $columns['modified'] = '最終更新日';
    $columns['title'] = 'イベント名称';

    return $columns;
}
add_filter( 'manage_edit-ivent_columns', 'add_columns_ivent' );



// ▼項目データ追加処理
function add_thumb_column_article( $column, $post_id ) {

    $strImg = '';

    switch ( $column ) {
        case 'state':
            echo get_field('state');
            break;
        case 'thumb':
            $thumb = wp_get_attachment_image_src(get_field('image'), 'thumbnail');
            // ロゴ画像がある場合
            if ($thumb[0] != '') {
                $strImg .= '<img src="'.$thumb[0].'" class="column-thumb" />';
            } else {
                $strImg .= '<img src="'.home_url('cms/img/noimage.png').'" class="column-thumb" />';
            }
            echo $strImg;
            break;
        // 最終更新日
        case 'modified':
            echo get_the_modified_date();
            break;
        default:
            break;
    }
}
add_action( 'manage_posts_custom_column', 'add_thumb_column_article', 10, 2 );


//-----------------------------------------------------
// ■カスタム投稿登録時使用関数
//-----------------------------------------------------
// ▼カスタム投稿設定
//-----------------------------------------------------
add_action( 'init', 'create_post_type' );
function create_post_type() {
    // 物件
    register_post_type( 'article',
        array(
            'labels' => array(
            'name' => __( '物件一覧・登録' ),
            'singular_name' => __( '物件一覧・登録' )
        ),
        'public' => true,
        'menu_icon' => 'dashicons-admin-home',
        'menu_position' =>5,
        'rewrite' => array('with_front' => false),
        'has_archive' => false,
        )
    );
    // カテゴリ
    register_taxonomy(
        'article_cat',  // タクソノミーのslug
        'article',        // 属する投稿タイプ
        array(
            'hierarchical' => true,
            'update_count_callback' => '_update_post_term_count',
            'label' => '物件一覧のカテゴリ',
            'singular_label' => '物件一覧のカテゴリ',
            'query_var' => true,
            'public' => true,
            'show_ui' => true
        )
    );

    // 建築事務所
    register_post_type( 'architect',
        array(
            'labels' => array(
            'name' => __( '建築事務所一覧・登録' ),
            'singular_name' => __( '建築事務所一覧・登録' )
        ),
        'public' => true,
        'menu_icon' => 'dashicons-hammer',
        'menu_position' =>6,
        'rewrite' => array('with_front' => true),
        'has_archive' => false,
        )
    );

    // イベント
    register_post_type( 'ivent',
        array(
            'labels' => array(
            'name' => __( 'イベント一覧・登録' ),
            'singular_name' => __( 'イベント一覧・登録' )
        ),
        'public' => true,
        'menu_icon' => 'dashicons-flag',
        'menu_position' =>7,
        'rewrite' => array('with_front' => false),
        'has_archive' => false,
        )
    );

        // 建築家
    register_post_type( // カスタム投稿タイプを定義するための関数
        'labo_architect', array(
            // ラベルの作成
            'labels' => array(
                'name' => '建築家', //管理画面などで表示する名前
                'singular_name' => __( '建築家' ), //管理画面などで表示する名前（単数形）
                'add_new_item'  => '建築家を追加', // 新規追加画面に表示される名前
                'edit_item'     => '建築家の編集', // 編集画面に表示される名前
                'view_item'     => '建築家', //編集ページの「投稿を表示」ボタンのラベル
                'search_items'  => '建築家の検索', //一覧ページの検索ボタンのラベル
                'not_found'     => '見つかりません。', //一覧ページに投稿が見つからなかったときに表示
                'not_found_in_trash' => 'ゴミ箱にはありません。' //ゴミ箱に何も入っていないときに表示
            ),
            'public' => true, // ダッシュボードに表示するか否か
            'hierarchical' => true, // 階層型にするか否か
            'has_archive' => true, // アーカイブを持つか否か
            'exclude_from_search' => false, //WPの検索機能から検索した際、検索対象に含めるか否かを設定（※trueの場合は検索対象に含めない）
            'supports' => array( //カスタム投稿ページで管理画面から投稿できる項目
                'title', // タイトル
                'editor' // 本文
                // 'thumbnail', // アイキャッチ画像
            ),
            'menu_position' => 5, // ダッシュボードで投稿の下に表示
            'rewrite' => array('with_front' => false), // パーマリンクの編集（shoplistの前の階層URLを消して表示）
        )
    );
    // カテゴリ
    register_taxonomy(
        'labo_architect_cat',  // タクソノミーのslug
        'labo_architect',        // 属する投稿タイプ
        array(
            'hierarchical' => true,
            'update_count_callback' => '_update_post_term_count',
            'label' => '建築家のカテゴリ',
            'singular_label' => '建築家のカテゴリ',
            'query_var' => true,
            'public' => true,
            'show_ui' => true
        )
    );

    // 工務店
    register_post_type( // カスタム投稿タイプを定義するための関数
        'labo_builder', array(
            // ラベルの作成
            'labels' => array(
                'name' => '工務店', //管理画面などで表示する名前
                'singular_name' => __( '工務店' ), //管理画面などで表示する名前（単数形）
                'add_new_item'  => '工務店を追加', // 新規追加画面に表示される名前
                'edit_item'     => '工務店の編集', // 編集画面に表示される名前
                'view_item'     => '工務店', //編集ページの「投稿を表示」ボタンのラベル
                'search_items'  => '工務店の検索', //一覧ページの検索ボタンのラベル
                'not_found'     => '見つかりません。', //一覧ページに投稿が見つからなかったときに表示
                'not_found_in_trash' => 'ゴミ箱にはありません。' //ゴミ箱に何も入っていないときに表示
            ),
            'public' => true, // ダッシュボードに表示するか否か
            'hierarchical' => true, // 階層型にするか否か
            'has_archive' => true, // アーカイブを持つか否か
            'exclude_from_search' => false, //WPの検索機能から検索した際、検索対象に含めるか否かを設定（※trueの場合は検索対象に含めない）
            'supports' => array( //カスタム投稿ページで管理画面から投稿できる項目
                'title', // タイトル
                'editor' // 本文
                // 'thumbnail', // アイキャッチ画像
            ),
            'menu_position' => 5, // ダッシュボードで投稿の下に表示
            'rewrite' => array('with_front' => false), // パーマリンクの編集（shoplistの前の階層URLを消して表示）
        )
    );
    // カテゴリ
    register_taxonomy(
        'labo_builder_cat',  // タクソノミーのslug
        'labo_builder',        // 属する投稿タイプ
        array(
            'hierarchical' => true,
            'update_count_callback' => '_update_post_term_count',
            'label' => '工務店のカテゴリ',
            'singular_label' => '工務店のカテゴリ',
            'query_var' => true,
            'public' => true,
            'show_ui' => true
        )
    );

    // マガジン
    register_post_type( // カスタム投稿タイプを定義するための関数
        'magazine', array(
            // ラベルの作成
            'labels' => array(
                'name' => 'マガジン', //管理画面などで表示する名前
                'singular_name' => __( 'マガジン' ), //管理画面などで表示する名前（単数形）
                'add_new_item'  => 'マガジンを追加', // 新規追加画面に表示される名前
                'edit_item'     => 'マガジンの編集', // 編集画面に表示される名前
                'view_item'     => 'マガジン', //編集ページの「投稿を表示」ボタンのラベル
                'search_items'  => 'マガジンの検索', //一覧ページの検索ボタンのラベル
                'not_found'     => '見つかりません。', //一覧ページに投稿が見つからなかったときに表示
                'not_found_in_trash' => 'ゴミ箱にはありません。' //ゴミ箱に何も入っていないときに表示
            ),
            'public' => true, // ダッシュボードに表示するか否か
            'hierarchical' => true, // 階層型にするか否か
            'has_archive' => true, // アーカイブを持つか否か
            'exclude_from_search' => false, //WPの検索機能から検索した際、検索対象に含めるか否かを設定（※trueの場合は検索対象に含めない）
            'supports' => array( //カスタム投稿ページで管理画面から投稿できる項目
                'title', // タイトル
                'thumbnail' // アイキャッチ画像
            ),
            'menu_position' => 9, // ダッシュボードで投稿の下に表示
            'rewrite' => array('with_front' => false), // パーマリンクの編集（shoplistの前の階層URLを消して表示）
        )
    );
    // カテゴリ
    register_taxonomy(
        'magazine_cat',  // タクソノミーのslug
        'magazine',        // 属する投稿タイプ
        array(
            'hierarchical' => true,
            'update_count_callback' => '_update_post_term_count',
            'label' => 'マガジンのカテゴリ',
            'singular_label' => 'マガジンのカテゴリ',
            'query_var' => true,
            'public' => true,
            'show_ui' => true
        )
    );
    // タグ
    register_taxonomy(
        'magazine_tag',  // タクソノミーのslug
        'magazine',         // 属する投稿タイプ
        array(
            'hierarchical' => true, // 管理画面右で一覧を出す場合に必要
            'update_count_callback' => '_update_post_term_count',
            'label' => 'マガジンのタグ',
            'singular_label' => 'マガジンのタグ',
            'public' => true,
            'show_ui' => true
        )
    );

    // ギャラリー
    register_post_type( // カスタム投稿タイプを定義するための関数
        'gallery', array(
            // ラベルの作成
            'labels' => array(
                'name' => 'ギャラリー', //管理画面などで表示する名前
                'singular_name' => __( 'ギャラリー' ), //管理画面などで表示する名前（単数形）
                'add_new_item'  => 'ギャラリーを追加', // 新規追加画面に表示される名前
                'edit_item'     => 'ギャラリーの編集', // 編集画面に表示される名前
                'view_item'     => 'ギャラリー', //編集ページの「投稿を表示」ボタンのラベル
                'search_items'  => 'ギャラリーの検索', //一覧ページの検索ボタンのラベル
                'not_found'     => '見つかりません。', //一覧ページに投稿が見つからなかったときに表示
                'not_found_in_trash' => 'ゴミ箱にはありません。' //ゴミ箱に何も入っていないときに表示
            ),
            'public' => true, // ダッシュボードに表示するか否か
            'hierarchical' => true, // 階層型にするか否か
            'has_archive' => true, // アーカイブを持つか否か
            'exclude_from_search' => false, //WPの検索機能から検索した際、検索対象に含めるか否かを設定（※trueの場合は検索対象に含めない）
            'supports' => array( //カスタム投稿ページで管理画面から投稿できる項目
                'title', // タイトル
                'editor' // 本文
                // 'thumbnail', // アイキャッチ画像
            ),
            'menu_position' => 10, // ダッシュボードで投稿の下に表示
            'rewrite' => array('with_front' => false), // パーマリンクの編集（shoplistの前の階層URLを消して表示）
        )
    );
    // カテゴリ
    register_taxonomy(
        'gallery_cat',  // タクソノミーのslug
        'gallery',        // 属する投稿タイプ
        array(
            'hierarchical' => true,
            'update_count_callback' => '_update_post_term_count',
            'label' => 'ギャラリーのカテゴリ',
            'singular_label' => 'ギャラリーのカテゴリ',
            'query_var' => true,
            'public' => true,
            'show_ui' => true
        )
    );

    // モデルハウス見学会
    register_post_type( // カスタム投稿タイプを定義するための関数
        'modelhouse', array(
            // ラベルの作成
            'labels' => array(
                'name' => 'モデルハウス見学会', //管理画面などで表示する名前
                'singular_name' => __( 'モデルハウス見学会' ), //管理画面などで表示する名前（単数形）
                'add_new_item'  => 'モデルハウス見学会を追加', // 新規追加画面に表示される名前
                'edit_item'     => 'モデルハウス見学会の編集', // 編集画面に表示される名前
                'view_item'     => 'モデルハウス見学会', //編集ページの「投稿を表示」ボタンのラベル
                'search_items'  => 'モデルハウス見学会の検索', //一覧ページの検索ボタンのラベル
                'not_found'     => '見つかりません。', //一覧ページに投稿が見つからなかったときに表示
                'not_found_in_trash' => 'ゴミ箱にはありません。' //ゴミ箱に何も入っていないときに表示
            ),
            'public' => true, // ダッシュボードに表示するか否か
            'hierarchical' => true, // 階層型にするか否か
            'has_archive' => true, // アーカイブを持つか否か
            'exclude_from_search' => false, //WPの検索機能から検索した際、検索対象に含めるか否かを設定（※trueの場合は検索対象に含めない）
            'supports' => array( //カスタム投稿ページで管理画面から投稿できる項目
                'title', // タイトル
                'editor', // 本文
                'thumbnail' // アイキャッチ画像
            ),
            'menu_position' => 10, // ダッシュボードで投稿の下に表示
            'rewrite' => array('with_front' => false), // パーマリンクの編集（shoplistの前の階層URLを消して表示）
        )
    );

}
add_theme_support('post-thumbnails');

// フィードフック一旦削除　投稿タイプを指定して表示件数を変更
function change_posts_per_page($query) {
    // 管理画面,メインクエリに干渉しないために必須
    if( is_admin() || ! $query->is_main_query() ){
        return;
    }

    if ( $query->is_post_type_archive('labo_architect') ) {
        $query->set( 'posts_per_page', '100' );
        return;
    }

    if ( $query->is_post_type_archive('magazine') ) {
        $query->set( 'posts_per_page', '9' );
        return;
    }

    if ( $query->is_post_type_archive('gallery') ) {
        $query->set( 'posts_per_page', '100' );
        return;
    }

    if ( $query->is_post_type_archive('modelhouse') ) {
        $query->set( 'posts_per_page', '6' );
        return;
    }

    if ( $query->is_archive() ) {
        $query->set( 'posts_per_page', '10' );
        return;
    }
}


// ▼タクソノミー追加
//-----------------------------------------------------
register_taxonomy(
    'article-kind', // 分類名
    'article',      // 投稿タイプ名
    array(
        'label' => '物件種別',
        'hierarchical' => true,
        'query_var' => true,
        'rewrite' => true
    )
);

register_taxonomy(
    'architect-type',
    'architect',
    array(
        'label' => 'タイプ',
        'hierarchical' => true,
        'query_var' => true,
        'rewrite' => true
    )
);

register_taxonomy(
    'ivent-type',
    'ivent',
    array(
        'label' => 'イベント種別',
        'hierarchical' => true,
        'query_var' => true,
        'rewrite' => true
    )
);



// ▼パーマリンクを数字設定へ
//-----------------------------------------------------

function my_post_type_link( $link, $post ){
    switch ($post->post_type) {
        case 'article':
            return home_url( '/article/' . $post->ID );
            break;
        case 'ivent':
            return home_url( '/ivent/' . $post->ID );
            break;
        default:
            return $link;
            break;
    }
}
add_filter( 'post_type_link', 'my_post_type_link', 1, 2 );

function my_rewrite_rules_array( $rules ) {
    $new_rules = array(
        'article/([0-9]+)/?$' => 'index.php?post_type=article&p=$matches[1]',
        'ivent/([0-9]+)/?$' => 'index.php?post_type=ivent&p=$matches[1]',
    );
    return $new_rules + $rules;
}
add_filter( 'rewrite_rules_array', 'my_rewrite_rules_array' );

// ▼固定ページのエディタ非表示
//-----------------------------------------------------
function remove_page_editor_support() {
    if (!current_user_can('level_10')) {
        remove_post_type_support( 'page', 'editor' );
        remove_post_type_support( 'article', 'editor' );
        remove_post_type_support( 'architect', 'editor' );
        remove_post_type_support( 'ivent', 'editor' );
    }
}
add_action( 'init' , 'remove_page_editor_support' );


//-----------------------------------------------------
// ■ページ表示時使用関数
//-----------------------------------------------------

// ▼wp_headの不要タグ除去
//-----------------------------------------------------
remove_action( 'wp_head', 'feed_links', 2 );
remove_action( 'wp_head', 'feed_links_extra', 3 );
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'index_rel_link' );
remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
remove_action( 'wp_head', 'wp_print_styles', 8 );
remove_action( 'wp_head', 'wp_print_head_scripts', 9 );
remove_action( 'wp_head', 'wp_generator' );
remove_action( 'wp_head', 'rel_canonical' );
remove_action( 'wp_head', 'rel_shortlink' );
remove_action( 'wp_head', 'wp_shortlink_wp_head');


// ▼郵便番号→住所変換
//-----------------------------------------------------
function my_print_footer_scripts() {
    global $post_type;

    // カスタム投稿の場合のみ
    if ($post_type == 'article'){
?>
<script type="text/javascript">
    jQuery(function($){
        // 郵便番号入力による住所自動入力
        $('#acf-field-zip').live('change', function(){
            var zip = $(this).val();
            var url = 'http://api.zipaddress.net?callback=?';
            var query = {'zipcode': zip};
            $.getJSON(url, query, function(json){
                $('#acf-field-address').val(json.fullAddress);
            });
        });
    });
</script>
<?php
    }
}
add_action('admin_print_footer_scripts', 'my_print_footer_scripts', 21);


// ▼固定ページスマートカスタムフィールドの表示
//-----------------------------------------------------
function add_meta_box_SCF( $settings, $post_type, $my_post, $meta_type) {
    if ($meta_type == 'post' && $post_type == 'page') {
        // Slug取得
        $page = get_page($my_post);
        $slug = $page->post_name;

        switch($slug) {
            case 'link':
                $set[] = $settings['106'];
                return $set;
                break;
            case 'home':
                $set[] = $settings['171'];
                return $set;
                break;
            case 'result':
                $set[] = $settings['187'];
                return $set;
                break;
            case 'ivent':
                // $set[] = $settings['578'];
                $set[] = $settings['582'];
                return $set;
                break;
        }
    } else {
        return $settings;
    }
}
add_filter( 'smart-cf-register-fields', 'add_meta_box_SCF', 10, 4 );


// ▼パンくず作成
//-----------------------------------------------------
function panlist() {
    global $post;
    $strTemp = '';
    $strTemp .= '<li><a href="'. home_url().'">HOME</a></li>';

    if( is_category() ){ //■カテゴリアーカイブ

        $strTemp .= '<li>フロムファースト高松・'.single_cat_title('', false).'</i>';

    } elseif(is_archive()) {
        // 投稿内容表示ページ
        $post_type = get_post_type($post);
        switch ($post_type) {
            case 'article':
                $strTemp .= '<li>フロムファースト高松・分譲地情報</li>';
                break;
            case 'architect':
                $strTemp .= '<li>フロムファースト高松・建築実績企業一覧</li>';
                break;
            case 'ivent';
                $strTemp .= '<li>フロムファースト高松・イベント情報一覧</li>';
                break;
        }
    } elseif(is_single()) {
        // 投稿内容表示ページ
        $post_type = get_post_type($post);

        switch ($post_type) {
            case 'post':
                $cat_now = get_the_category();
                $cat_now = $cat_now[0]->cat_ID;
                break;
            case 'article':
                $strTemp .= '<li><a href="'.home_url('article-map').'">フロムファースト高松・分譲地情報</a></li>';
                break;
            case 'architect':
                $strTemp .= '<li><a href="'.home_url('architect').'">フロムファースト高松・建築実績企業</a></li>';
                break;
            case 'ivent';
                $strTemp .= '<li><a href="'.home_url('ivent').'">フロムファースト高松・イベント情報一覧</a></li>';
                break;
        }
        $strTemp .= '<li>'.get_the_title().'</li>';

    } elseif(is_page()){
        $strTemp .= '<li>'.get_field('seo_title').'</li>';
    } elseif(is_404()){
        // 404ページ
        $strTemp .= '<li>ページが見つかりません。</li>';
    }
    return $strTemp;
}


// ▼ユーザーエージェント確認、スマートフォン振り分け
//-----------------------------------------------------
function check_useragent(){
    $user_agent = $_SERVER['HTTP_USER_AGENT'];
    $temp_flg = false;

    if ( strpos( $user_agent, 'iPhone' ) !== false || strpos( $user_agent, 'Android' ) !== false || strpos( $user_agent, 'iPod' ) !== false ){
        $temp_flg = true;
    }
    return $temp_flg;
}


// ▼各項目名称変換
// @param $flg (c:価格、m:面積、p：パーセント、k：階建て、s：文字列、p_k：文字列パーセント)
// @param $arg
//-----------------------------------------------------
function return_nm($arg, $flg) {
    $temp = '';
    switch($flg) {
        case 'c':
            if ($arg > 0) {
                $arg = $arg * 10000;

                if ($arg < 10000) {
                    $temp = '<span>'.number_format($arg) . '</span> 円';
                } else {
                    if ($arg % 10000 === 0) {
                        $temp = sprintf('%5.0f', $arg / 10000);
                        $temp = number_format($temp, 2);
                        $temp = preg_replace("/\.?0+$/","",$temp) . ' 万円';
                    } else {
                        $temp = sprintf('%5.2f', $arg / 10000);
                        $temp = number_format($temp, 2);
                        $temp = preg_replace("/\.?0+$/","",$temp) . ' 万円';
                    }
                }
            }
            break;
        case 'm':
            if ($arg != 0) {
                $temp = $arg . '㎡';
            } else {
                $temp = '--';
            }
            break;
        case 'p':
            if ($arg != 0 && $arg != '') {
                $temp = $arg . '%';
            } else {
                $temp = '--';
            }
            break;
        case 'tubo':
            if ($arg != '') {
                $temp = $arg . '坪';
            } else {
                $temp = '';
            }
            break;
        case 's':
            if ($arg != '') {
                $temp = $arg;
            } else {
                $temp = '--';
            }
            break;
        case 'kukaku':
            if ($arg != 0) {
                $temp = $arg.'区画';
            } else {
                $temp = '--';
            }
            break;
    }
    return $temp;
}

// ▼坪数計算(㎡→坪へ変換)
// @param $arg
// @param $flg
//------------------------------
function squareChange($arg, $flg) {
    $tubo = '';
    if (is_numeric($arg)) {
        $int = $arg * 0.3025;
        // $int = floor($int * 100) / 100;  // 少数第３位を切り捨て
        $int = round($int, 1);      // 少数第２位を四捨五入


        if ($int > 0) {
            if ($flg) {
                $tubo = strval($int);
            } else {
                $tubo = '(' . strval($int) . '坪)';
            }
        }
    }
    return $tubo;
}

// ▼面積計算(坪→㎡へ変換)
// @param $arg
// @param $flg
//------------------------------
function tuboChange($arg) {
    $square = '';
    if (is_numeric($arg)) {
        $int = $arg / 0.3025;
        // $int = floor($int * 100) / 100;  // 少数第3位を切り捨て
        $int = round($int, 1);      // 少数第２位を四捨五入

        if ($int > 0) {
            $square = strval($int) . 'm<sup>2</sup>';
        }
    }
    return $square;
}


// ▼データ無し用HTMLコード
//-----------------------------------------------------
function dataNotfound () {
    $tmp = '';

    $tmp .= '<div class="clearfix notfound mt30 mb10">'."\n";
    $tmp .= '<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">'."\n";
    $tmp .= '<div class="row">'."\n";
    $tmp .= '<img src="'.THEME_URL.'/img/icon.png" alt="ページが見つかりませんでした。" class="img-responsive center-block">'."\n";
    $tmp .= '</div>'."\n";
    $tmp .= '</div>'."\n";
    $tmp .= '<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 mb10">'."\n";
    $tmp .= '<div class="row">'."\n";
    $tmp .= '<h3>Data Not Found</h3>'."\n";
    $tmp .= '<p class="ft_size15">情報が登録されていません。</p>'."\n";
    $tmp .= '<p>'."\n";
    $tmp .= '現在、ご覧いただいているページへデータの登録がされておりません。<br />'."\n";
    $tmp .= '削除、または非掲載になったことが考えられます。<br />'."\n";
    $tmp .= 'また後程ご確認下さい。'."\n";
    $tmp .= '</p>'."\n";
    $tmp .= '</div>'."\n";
    $tmp .= '</div>'."\n";
    $tmp .= '</div>'."\n";
    $tmp .= '<p class="text-center"><a href="'.home_url().'" class="btn btn-primary">HOMEへ戻る</a></p>'."\n";

    return $tmp;
}

// Customize YouTube oEmbed Code
function custom_youtube_oembed($code){
  if(strpos($code, 'youtu.be') !== false || strpos($code, 'youtube.com') !== false){
    $html = preg_replace("@src=(['\"])?([^'\">\s]*)@", "src=$1$2&rel=0", $code);
    $html = preg_replace('/ width="\d+"/', '', $html);
    $html = preg_replace('/ height="\d+"/', '', $html);
    $html = '<div class="VideoWrapper">' . $html . '</div>';

    return $html;
  }
  return $code;
}
