<?php
    /*

    # ループ処理の終了

    ## 使用方法

    include ./templates/php/loopStart.php
    ~ 記事の出力処理 ~
    include ./templates/php/loopEndWhile.php
    | <? php else: ?>
    ~ 記事が登録されていなかった場合の処理 ~
    | <? php endif; ?>

    ### 注意事項
    loopStart及びloopStart_wpqueryとセットで使用すること

    */

    endwhile;
?>
