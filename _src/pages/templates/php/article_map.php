<?php
//********************************************************************
//* Title       フロムファースト　地図から物件を探すテンプレート
//* Copyright   Bios
//* Code author koumoto <koumoto@casablanca-net.co.jp>
//********************************************************************


    // データ検索
    $args = array(
        'numberposts' => -1,
        'post_type' => 'article'
    );
    global $post;
    $posts = get_posts($args);

    // 表示物件データ整形
    $aryArticle = array();
    $aryType = array();

    if($posts) {
        foreach($posts as $post){

            $arytemp = array();
            $arytemp['image'] = get_field('image');
            $arytemp['image'] = wp_get_attachment_image_src($arytemp['image'],'medium');

            // 画像登録無し
            if ($arytemp['image'][0] == '') {
                $arytemp['image'][0] = home_url('cms/wp-content/themes/official/assets/images/article/no-image.png');
            }

            $arytemp['name'] = get_field('name');
            $arytemp['state'] = get_field('state');
            $arytemp['address'] = get_field('address');
            $arytemp['link'] = get_permalink();
            $arytemp['map-x'] = get_field('map-x');
            $arytemp['map-y'] = get_field('map-y');

            $arytemp['class'] = '';
            switch($arytemp['state']) {
                case '分譲中':
                    $arytemp['class'] = 'state1';
                    break;
                case '造成途中':
                case '近日造成':
                    $arytemp['class'] = 'state4';
                    break;
                case '計画中':
                    $arytemp['class'] = 'state2';
                    break;
                case '完売御礼':
                    $arytemp['class'] = 'state3';
                    break;
            }

            // ▽区画情報
            $aryKukaku = array();
            $aryKukaku = SCF::get('kukaku');

            // 区画数
            $intKukaku = 0;
            $intKukaku = count($aryKukaku);
            if ($intKukaku == 1) {
                if ($aryKukaku[0]['k-no'] == '') {
                    $intKukaku = 0;
                }
            }

            // 坪数の最大値、最小値を計測する為に配列に
            $intKukakuSquere = '';
            $aryKukakuSquere = array();
            foreach ($aryKukaku as $key => $value) {
                $aryKukakuSquere[] = $value['k-square'];
            }

            if ($intKukaku == 1) {
                $intKukakuSquere = return_nm(max($aryKukakuSquere),'tubo');
            } elseif ($intKukaku == 0) {
                $intKukakuSquere = '--';
            } else {
                $intKukakuSquere = return_nm(min($aryKukakuSquere),'tubo').' ～ '.return_nm(max($aryKukakuSquere),'tubo');
            }
            $arytemp['kukaku'] = '区画面積:'.$intKukakuSquere;

            array_push($aryArticle, $arytemp);


            // 各状態をキーにした配列に格納
            $tmp = $arytemp['class'];
            if ($tmp == 'state4') {
                $tmp = 'state1';
            }
            if (isset($aryType[$tmp])) {
                array_push($aryType[$tmp], $arytemp);
            } else {
                $aryType[$tmp][0] = $arytemp;
            }

        }
    }
    wp_reset_query();   //Query Reset

    // 地図設定
    $strtable = '';


    if (count($aryArticle) > 0) {
        // for ($y=0; $y<50; $y++) {
        for ($y=0; $y<85; $y++) {
            $strtable .= '<tr>'."\n";
            // for ($x=0; $x<55; $x++) {
            for ($x=0; $x<90; $x++) {
                foreach($aryArticle as $key => $value){
                    $map_x = $value['map-x'];
                    $map_y = $value['map-y'];
                    $strTemp = '';

                    if ($x == $map_x && $y == $map_y) {
                        switch($value['state']) {
                            case '分譲中':
                            case '造成途中':
                            case '近日造成':
                                $strTemp .= '<a href="'.$value['link'].'"><img src="'.THEME_URL.'/assets/images/article/list1.png" alt="分譲中" class="m_state1_img img_hover" /></a>'."\n";
                                break;
                            case '計画中':
                                $strTemp .= '<a href="'.$value['link'].'"><img src="'.THEME_URL.'/assets/images/article/list3.png" alt="計画中" class="m_state2_img img_hover" /></a>'."\n";
                                break;
                            case '完売御礼':
                                $strTemp .= '<a href="'.$value['link'].'"><img src="'.THEME_URL.'/assets/images/article/list2.png" alt="完売御礼" class="m_state3_img img_hover" /></a>'."\n";
                                break;

                            /*
                            case '分譲中':
                            case '造成途中':
                            case '近日造成':
                                $strTemp .= '<img src="'.THEME_URL.'/img/list/osipin.gif" alt="分譲中" class="m_state1_img" />'."\n";
                                break;
                            case '計画中':
                                $strTemp .= '<img src="'.THEME_URL.'/img/list/osipin3.gif" alt="計画中" class="m_state2_img" />'."\n";
                                break;
                            case '完売御礼':
                                $strTemp .= '<img src="'.THEME_URL.'/img/list/osipin2.gif" alt="完売御礼" class="m_state3_img" />'."\n";
                                break;
                            */
                        }
                        break;
                    }
                }
                $strtable .= '<td>'.$strTemp.'</td>'."\n";
            }
            $strtable .= '</tr>'."\n";
        }
    }
?>
<?php
    if (count($aryArticle) != 0 ) {
?>
        <div class="clearfix">
            <ul class="list-inline pull-right list_markup">
                <li class="li2"> <input type="checkbox" id="m_state1" checked="checked" /> <a href="#state1">現在販売中物件</a></li>
                <li class="li1"> <input type="checkbox" id="m_state2" checked="checked" /> <a href="#state2">現在計画中物件</a></li>
                <li class="li3"> <input type="checkbox" id="m_state3" checked="checked" /> <a href="#state3">完売御礼物件</a></li>
            </ul>
        </div>
        <div class="table-responsive">
            <table class="tbl-listmap">
                <?= $strtable ?>
            </table>
        </div>

        <div class="google-mymap">
            <div class="icon-list">
                <ul class="list">
                    <li class="item sale">
                        <p>販売中</p>
                    </li>
                    <li class="item plan">
                        <p>計画中</p>
                    </li>
                    <li class="item sold">
                        <p>完売済み</p>
                    </li>
                </ul>
            </div>
            <div class="map-content">
                <div class="for-large">
                    <iframe src="https://www.google.com/maps/d/u/2/embed?mid=1NurLqgcMWeJIdHHasMkPbiCdDMBdBAf7&ll=34.319145978965906%2C134.03802811461972&z=13"></iframe>
                </div>
                <div class="for-small">
                    <iframe src="https://www.google.com/maps/d/u/2/embed?mid=1NurLqgcMWeJIdHHasMkPbiCdDMBdBAf7&ll=34.30893465652222%2C134.0393584902911&z=11"></iframe>
                </div>
            </div>
            <a class="countion" href="https://www.google.co.jp/maps/@34.3101526,133.9923574,13z/data=!4m2!6m1!1s1NurLqgcMWeJIdHHasMkPbiCdDMBdBAf7?hl=ja&authuser=0" target="_blank">
                <p class="attend">完売済みの物件は<br class="for-small">Googleマップのストリートビューより<br class="for-midi">分譲地内の街並みをご覧頂けます。</p>
                <div class="button-flat">
                    <span>Googleマップで開く
                        <div class="arrow">
                            <div class="for-large">
                                <svg version="1.1" id="レイヤー_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewbox="0 0 60 6" style="enable-background:new 0 0 60 6;" xml:space="preserve">
                                    <polygon class="st0" points="46.4,0 46,0.9 55.2,5 0,5 0,6 60,6 "></polygon>
                                </svg>
                            </div>
                            <div class="for-small">
                                <svg version="1.1" id="レイヤー_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewbox="0 0 23 6" style="enable-background:new 0 0 23 6;" xml:space="preserve">
                                    <polygon class="st0" points="23,6 0,6 0,5 18.1,5 8.8,0.9 9.2,0 "></polygon>
                                </svg>
                            </div>
                        </div>
                    </span>
                </div>
            </a>
        </div>
<?php
        foreach ($aryType as $key => $value) {
            switch($key) {
                case 'state1':
                case 'state4':
                    $strTemp = '現在販売中物件';
                    break;
                case 'state2':
                    $strTemp = '現在計画中物件';
                    break;
                case 'state3':
                    $strTemp = '完売御礼物件';
                    break;
            }
?>
            <div class="bs-callout bs-callout-info" id="<?= $key ?>">
                <h4><?= $strTemp ?></h4>
            </div>
            <div class="clearfix">
<?php
                foreach ($value as $key_e => $value_e ){
?>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 list-block mb20">
                        <div class="row">
                            <div class="thumbnail hover_block">
                                <a href="<?= $value_e['link'] ?>">
                                <div class="clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                        <div class="row">
                                            <img src="<?= $value_e['image'][0] ?>" alt="<?= $value_e['name'] ?>" class="img-responsive center-block">
                                        </div>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                        <div class="row pl05">
                                            <p class="pt05"><span class="state <?= $value_e['class'] ?>"><?= $value_e['state'] ?></span></p>
                                            <p class="ft_color2"><?= $value_e['name'] ?></p>
                                            <p><?= $value_e['kukaku'] ?></p>
                                        </div>
                                    </div>
                                </div>
                                </a>
                            </div>
                        </div>
                    </div>
<?php
                }
?>
            </div>
<?php
        }
    } else {
        // NOTFOUND
        echo dataNotfound();
    }
?>
