<?php
    /*

    # ループ処理をリセットする

    1ページ中に複数回ループ処理を入れる場合に使用をする。

    例
    トップページで新着情報と施工事例のループを使用する場合は、それぞれのループ終了後に記述を入れるなど。

    ## 参考URL
    https://wpdocs.osdn.jp/%E9%96%A2%E6%95%B0%E3%83%AA%E3%83%95%E3%82%A1%E3%83%AC%E3%83%B3%E3%82%B9/wp_reset_postdata

    ## 使用方法

    include ./templates/php/reset.php

    */

    wp_reset_postdata();
?>
