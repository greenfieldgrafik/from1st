<?php
    /*

    # ループ処理の終了

    ## 使用方法

    include ./templates/php/loopStart.php
    ~ 記事の出力処理 ~
    include ./templates/php/loopEnd.php

    ### 注意事項
    loopStart及びloopStart_wpqueryとセットで使用すること

    */

    endwhile; endif;
?>
