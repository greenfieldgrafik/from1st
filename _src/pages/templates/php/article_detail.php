<?php
//********************************************************************
//* Title       フロムファースト　物件詳細テンプレート
//* Copyright   Bios
//* Code author koumoto <koumoto@casablanca-net.co.jp>
//********************************************************************

    global $glo_aryzimage;

    $aryHtml = array();
    $flgkukaku = true;      //　区画が登録されていない場合でも区画配列のカウントは1になる為、その判別

    // ▽区画図
    $strImage = '';
    $strImage = wp_get_attachment_image_src(get_field('image'), 'large');

    if ($strImage[0] == '') {
        $strImage = '<img src="'.home_url('wp/wp-content/themes/official/assets/images/article/no-image.png').'" alt="'.get_the_title().'区画図" class="kukakuimg center-block" />'."\n";
    } else {
        $strImage = '<a href="'. $strImage[0].'"><img src="'.$strImage[0].'" alt="'.get_the_title().'区画図" class="kukakuimg center-block" /></a>'."\n";
    }

    // ▽造成画像
    $aryZImage = array();
    $aryZImage[0] = wp_get_attachment_image_src(get_field('z_image1'), 'large');
    $aryZImage[1] = wp_get_attachment_image_src(get_field('z_image2'), 'large');
    $aryZImage[2] = wp_get_attachment_image_src(get_field('z_image3'), 'large');
    $aryZImage[3] = wp_get_attachment_image_src(get_field('z_image4'), 'large');

    $aryZImage[0] = $aryZImage[0][0];
    $aryZImage[1] = $aryZImage[1][0];
    $aryZImage[2] = $aryZImage[2][0];
    $aryZImage[3] = $aryZImage[3][0];


    // ▽区画情報
    $aryKukaku = array();
    $aryKukaku = SCF::get('kukaku');

    // 区画数
    $intKukaku = 0;
    $intKukaku = count($aryKukaku);
    if ($intKukaku == 1) {
        if ($aryKukaku[0]['k-no'] == '') {
            $intKukaku = 0;
            $flgkukaku = false;
        }
    }

    // 坪数の最大値、最小値を計測する為に配列に
    $intKukakuSquere = '';
    $aryKukakuSquere = array();
    foreach ($aryKukaku as $key => $value) {
        $aryKukakuSquere[] = $value['k-square'];
    }

    if ($intKukaku == 1) {
        $intKukakuSquere = return_nm(max($aryKukakuSquere),'tubo');
    } elseif ($intKukaku == 0) {
        $intKukakuSquere = '--';
    } else {
        $intKukakuSquere = return_nm(min($aryKukakuSquere),'tubo').' ～ '.return_nm(max($aryKukakuSquere),'tubo');
    }



    // ▽PDF
    $strPdf = '';
    $aryPdfName = array('1'=>'丈量図','2'=>'造成計画平面図','3'=>'造成断面図','4'=>'利用計画図','5'=>'測量図','6'=>'開発許可書','7'=>'検査済証');
    for ($i=1; $i < 8; $i++) {
        $tmp = wp_get_attachment_url(get_field('pdf'.$i), 'full');
        if ($tmp != '') {
            $strPdf .= '<li><a href="'.$tmp.'" target="_blank">'.$aryPdfName[$i].'</a></li>'."\n";
        }
    }

    if ($strPdf != '') {
        $strTemp = '';
        $strTemp .= '<div class="mt20 blockpdf">'."\n";
        $strTemp .= '<p class="mb05 ft_size12 ft_color3">'."\n";
        $strTemp .= '<img src="'.THEME_URL.'/assets/images/article/pdf.png" alt="PDF"> 以下のファイルをダウンロード出来ます。'."\n";
        $strTemp .= '</p>'."\n";
        $strTemp .= '<ul class="list-inline mt20 mb10 list_pdf">'."\n";
        $strTemp .= $strPdf;
        $strTemp .= '</ul>'."\n";
        $strTemp .= '</div>'."\n";

        $strPdf = $strTemp;
    }

?>
    <h2 class="a-detail-title"><?= get_the_title() ?></h2>
    <p class="bg_title7 ft_color1 pl30"><strong><?= get_field('title') ?></strong></p>
<?php
    $strclass = '';
    switch(get_field('state')) {
        case '分譲中':
            $strclass = 'state1';
            break;
        case '造成途中':
        case '近日造成':
            $strclass = 'state4';
            break;
        case '計画中':
            $strclass = 'state2';
            break;
        case '完売御礼':
            $strclass = 'state3';
            break;
    }
?>
    <p class="pt30 pb20 text-center"><span class="state <?= $strclass ?> ft_size22"><?= get_field('state') ?></span></p>
    <?php if ( is_single( array( 10865, 14692) ) ): ?>
    <p class="mt20 mb30 text-center emphasis"><?= get_field('pr') ?></p>
    <?php else: ?>
    <p class="mt20 mb30 ft_color2 ft_size15 text-center"><?= get_field('pr') ?></p>
    <?php endif; ?>
    <div class="clearfix">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 mb30 kukakuzu">
            <p class="mb10"><span class="highlite">区画図</span></p>
            <span id="lightbox"><?= $strImage ?></span>
            <?= $strPdf ?>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 mb30 bukken_g">
            <p class="mb10"><span class="highlite">物件概要</span></p>
            <table class="table table-bordered tb_detail mb30" summary="基本情報">
                <tbody>
                <tr>
                    <th>所在地</th><td><?= get_field('address') ?></td>
                    <th>区画数</th><td><?= return_nm($intKukaku, 'kukaku') ?></td>
                </tr>
                <tr>
                    <th>地目</th><td><?= return_nm(get_field('land'), 's') ?></td>
                    <th>区画面積</th><td><?= $intKukakuSquere ?></td>
                </tr>
                <tr>
                    <th>都市計画</th><td><?= return_nm(get_field('cityplan'), 's') ?></td>
                    <th>校区</th><td><?= return_nm(get_field('school1'), 's') ?>/<?= return_nm(get_field('school2'),'s') ?></td>
                </tr>
                <tr>
                    <th>容積率</th><td><?= return_nm(get_field('float2'), 'p') ?></td>
                    <th>建ぺい率</th><td><?= return_nm(get_field('float1'), 'p') ?></td>
                </tr>
                <tr>
                    <th>売主</th><td><?= return_nm(get_field('buyer'), 's') ?></td>
                    <th>販売代理</th><td><?= return_nm(get_field('agent'),'s') ?></td>
                </tr>
                </tbody>
            </table>
<?php
        if( get_field('youtube') ){
                echo $embed_code =custom_youtube_oembed(wp_oembed_get(get_field('youtube')));
                }
?>
        </div>
    </div>

    <hr class="mb20" />
<?php
    if ($flgkukaku === true) {
?>
        <p class="mb10"><span class="highlite">区画面積表</span></p>
        <div class="table-responsive">
            <table class="table table-bordered tb_kukaku ft_size12" summary="区画情報">
                <tr>
                    <th></th>
                    <th>号地</th>
                    <th>宅地面積(m<sup>2</sup>)</th>
                    <th>坪数(坪)</th>
                    <th>坪単価(万円)</th>
                    <th>販売価格(万円)</th>
                    <th>動画1</th>
                    <th>動画2</th>
                </tr>
<?php
        $i = 0;
        foreach ($aryKukaku as $key => $value) {

            switch($value['k-state']) {
                case '引渡済':
                    echo '<tr class="bg_color4_k eleseline1">';
                    // echo '<td><img src="'.THEME_URL.'/img/detail/mark1.png" /></td>'."\n";
                    echo '<td>'.$value['k-state'].'</td>'."\n";
                    break;
                case '契約済':
                    echo '<tr class="bg_color3_k eleseline2">';
                    // echo '<td><img src="'.THEME_URL.'/img/detail/mark2.png" /></td>'."\n";
                    echo '<td class="ft_color3_k">'.$value['k-state'].'</td>'."\n";
                    break;
                case '予約中':
                    echo '<tr class="bg_color2_k">';
                    // echo '<td><img src="'.THEME_URL.'/img/detail/mark3.png" /></td>'."\n";
                    echo '<td class="ft_color2_k">'.$value['k-state'].'</td>'."\n";
                    break;
                case '商談中':
                    echo '<tr class="bg_color1_k">';
                    // echo '<td><img src="'.THEME_URL.'/img/detail/mark4.png" /></td>'."\n";
                    echo '<td class="ft_color1_k">'.$value['k-state'].'</td>'."\n";
                    break;
                case '募集地':
                    echo '<tr class="bg_color5_k">';
                    // echo '<td> </td>'."\n";
                    echo '<td class="ft_color6_k">'.$value['k-state'].'</td>'."\n";
                    break;
                case '販売中':
                    echo '<tr class="bg_color5_k">';
                    // echo '<td> </td>'."\n";
                    echo '<td class="ft_color6_k">'.$value['k-state'].'</td>'."\n";
                    break;
                case '空き':
                    echo '<tr>';
                    // echo '<td><img src="'.THEME_URL.'/img/detail/mark5.png" /></td>'."\n";
                    echo '<td> </td>'."\n";
                    break;
                default:
                    echo '<tr>';
                    // echo '<td> </td>'."\n";
                    echo '<td> </td>'."\n";
                    break;
            }

            echo '<td>'.$value['k-no'].'</td>'."\n";
            echo '<td>'.tuboChange($value['k-square']).'</td>'."\n";
            echo '<td>'.return_nm($value['k-square'], 'tubo').'</td>'."\n";

            switch($value['k-state']) {
                case '引渡済':
                    echo '<td>'.$value['k-state'].'</td>'."\n";
                    echo '<td>'.$value['k-state'].'</td>'."\n";
                    break;
                default:
                    $tmp = $value['k-cost'] / $value['k-square'];
                    $tmp = round($tmp, 1);  // 少数2位を四捨五入
                    echo '<td>'.return_nm($tmp, 'c').'</td>'."\n";
                    echo '<td>'.return_nm($value['k-cost'], 'c').'</td>'."\n";
                    break;
            }

            if ($value['k-youtube1'] == '') {
                echo '<td></td>'."\n";
            } else {
                echo '<td><a href="'.$value['k-youtube1'].'" target="_blank"><img src="'.THEME_URL.'/assets/images/article/yt_icon_rgb.png" alt="動画はこちら" height="20" class="center-block"></a></td>'."\n";
            }

            if ($value['k-youtube2'] == '') {
                echo '<td></td>'."\n";
            } else {
                echo '<td><a href="'.$value['k-youtube2'].'" target="_blank"><img src="'.THEME_URL.'/assets/images/article/yt_icon_rgb.png" alt="動画はこちら" height="20" class="center-block"></a></td>'."\n";
            }



            echo '</tr>';
        }
?>
            </table>
        </div>
    <hr />
    <div class="clearfix mt40 mb40 skyshot">

<?php
        $flg_skyshot = false;
        foreach ($aryZImage as $key => $value) {
            if ($value != '') {
                if ($flg_skyshot === false) {
?>
                <div class="skyshot_b mb20">
                    <img src="<?= $value ?>" alt="空撮画像" class="img-responsive center-block" id="img1" />
                </div>
<?php
                }
?>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 mb20">
                    <p><img src="<?= $value ?>" alt="<?= $glo_aryzimage[$key] ?>画像" class="img-responsive center-block" /></p>
                </div>
<?php
                $flg_skyshot = true;
            }
        }
?>
    </div>

<?php
    }
    $frame_temp = get_field('map_frame');
    if ($frame_temp != '') {
?>
        <hr />
        <p class="mb10"><span class="highlite">地図</span>

        </p>



        <div class="map_frame">
            <?= htmlspecialchars_decode ($frame_temp) ?>
        </div>


<?php

        echo '<a href="https://www.google.com/maps?q='.get_field('map_code').'" target="_blank"><img src="'.THEME_URL.'/assets/images/article/btn-gmap.png" alt="Google Mapで開く" class="img-responsive"></a>'."\n";

    }
/*
    $map_temp = get_field('map');
    if ($map_temp['lat'] != '' && $map_temp['lng'] != '') {
?>
        <hr />
        <p class="mb10"><span class="highlite">地図</span></p>
        <div id="map" class="mb30"></div>
        <script>
        <!--//
        $(function(){
            var map     = null;
            var latlng  = null;
            var marker  = null;
            var option  = null;
        <?php
            if ($map_temp['lat'] == '' && $map_temp['lng'] == ''){
        ?>
                return false;
        <?php
            }
        ?>
            latlng = new google.maps.LatLng(<?= $map_temp['lat'] ?>, <?= $map_temp['lng'] ?>);
            option = {
                center: latlng,
                zoom: 16,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById('map'), option);
            marker = new google.maps.Marker({
                position: latlng,
                map: map
            });
        });
        -->
        </script>
<?php
    }
*/
    echo '<div class="noprint">'."\n";
    get_template_part('parts', 'detail_architect');
    echo '</div>'."\n";
?>

