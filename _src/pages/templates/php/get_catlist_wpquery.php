<?php
    /*

    # ターム一覧（カスタム投稿タイプで作成したカテゴリー）を表示する

    ## 参考URL
    https://wpdocs.osdn.jp/%E9%96%A2%E6%95%B0%E3%83%AA%E3%83%95%E3%82%A1%E3%83%AC%E3%83%B3%E3%82%B9/get_terms

    ## 使用方法

    ul
        | <?php $taxonomyName = 'blog_cat'; ?>
        include ./templates/php/get_catlist_wpquery.php

    */

    $taxonomy = $taxonomyName; // カスタム分類名

    $args = array(
        'fields'     => 'all',
        'hide_empty' => true // カテゴリーに投稿が含まれていない場合は、カテゴリーを表示
    );
    $terms = get_terms( $taxonomy, $args);
    foreach($terms as $term):
    $term = sanitize_term( $term, $taxonomy );
    $term_link = esc_url(get_term_link( $term, $taxonomy ));
    if ( is_wp_error( $term_link ) ) {
        continue;
    }
 ?>
<li><a class="<?php if (is_tax($taxonomy, $term->slug)) { echo 'current'; } ?>" href="<?php echo $term_link; ?>"><?php echo $term->name; ?></a></li>
<?php endforeach; ?>
