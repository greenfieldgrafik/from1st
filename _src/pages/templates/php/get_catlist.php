<?php
    /*

    # カテゴリー一覧を表示する

    ## 参考URL
    https://wpdocs.osdn.jp/%E9%96%A2%E6%95%B0%E3%83%AA%E3%83%95%E3%82%A1%E3%83%AC%E3%83%B3%E3%82%B9/get_terms

    ## 使用方法

    ul
        include ./templates/php/get_catlist.php

    */

    $args = array(
        'fields'     => 'all',
        'hide_empty' => true, // カテゴリーに投稿が含まれていない場合は、カテゴリーを表示
        'exclude'     => '60,61'
    );

    $cat_all = get_terms( "category", $args );
    foreach($cat_all as $value):
 ?>
<li class="-item"><a class="<?php if (is_category($value->slug)) { echo 'current'; } ?>" href="<?php echo get_category_link($value->term_id); ?>"><span><?php echo $value->name; ?></span></a></li>
<?php endforeach; ?>
