<?php

    // 中カテゴリのループ
    $terms = get_the_terms($post->ID,'gallery_cat');

    foreach((array)$terms as $term1) :
        $currentCatSlug = $term1->slug;
        $currentCatTitle = $term1->name;
        $currentCatDescription = $term1->description;
        $currentCatId = $term1 -> term_id;

        // 下の二つはスマートカスタムフィールド用
        $p_name_ja = SCF::get_term_meta( $currentCatId, 'gallery_cat', 'p_name_ja' );
        $p_name_en = SCF::get_term_meta( $currentCatId, 'gallery_cat', 'p_name_en' );
        $p_company = SCF::get_term_meta( $currentCatId, 'gallery_cat', 'p_company' );
        $p_company_en = SCF::get_term_meta( $currentCatId, 'gallery_cat', 'p_company_en' );
        $p_portrate = SCF::get_term_meta( $currentCatId, 'gallery_cat', 'p_portrate' );
        $portrate_image = wp_get_attachment_image($p_portrate, 'full');
        $p_career = SCF::get_term_meta( $currentCatId, 'gallery_cat', 'p_career' );
        $p_about = SCF::get_term_meta( $currentCatId, 'gallery_cat', '会社概要' );
        $p_site = SCF::get_term_meta( $currentCatId, 'gallery_cat', 'URL' );
        $p_awards = SCF::get_term_meta( $currentCatId, 'gallery_cat', 'p_awards' );
        $p_link = SCF::get_term_meta( $currentCatId, 'gallery_cat', 'p_link' );
        $p_movie = SCF::get_term_meta( $currentCatId, 'gallery_cat', 'p_movie' );
    endforeach;

?>
