<?php

//タクソノミー名
$taxonomy = "gallery_cat";

//親タームだとparentが0なのでこれを指定
$args = array(
    'parent' => 0,
    'orderby' => 'term_order',
    'hide_empty' => false,
    'slug' => 'g_architect',
);

//親を取得
$terms = get_terms($taxonomy, $args);
foreach ((array)$terms as $term) {

    $term = sanitize_term( $term, $taxonomy );
    $term_link = get_term_link( $term, $taxonomy );

    //親のIDを取得
    $parentId = $term -> term_id;

?>
