<?php
    $arytemp = array();
    $arytemp['image'] = get_field('image');
    $arytemp['image'] = wp_get_attachment_image_src($arytemp['image'],'large');
    $arytemp['name'] = get_field('name');
    if ($arytemp['image'][0] == '') {
    $arytemp['image'][0] = home_url('cms/wp-content/themes/official/assets/images/article/no-image.png');
    }
    $arytemp['state'] = get_field('state');

    $arytemp['class'] = '';
    switch($arytemp['state']) {
        case '分譲中':
            $arytemp['class'] = 'state1';
            break;
        case '造成途中':
        case '近日造成':
            $arytemp['class'] = 'state4';
            break;
        case '計画中':
            $arytemp['class'] = 'state2';
            break;
        case '完売御礼':
            $arytemp['class'] = 'state3';
            break;
    }

    // ▽区画情報
    $aryKukaku = array();
    $aryKukaku = SCF::get('kukaku');

    // 区画数
    $intKukaku = 0;
    $intKukaku = count($aryKukaku);
    if ($intKukaku == 1) {
        if ($aryKukaku[0]['k-no'] == '') {
            $intKukaku = 0;
        }
    }

    // 坪数の最大値、最小値を計測する為に配列に
    $intKukakuSquere = '';
    $aryKukakuSquere = array();
    foreach ($aryKukaku as $key_k => $value_k) {
        $aryKukakuSquere[] = $value_k['k-square'];
    }

    if ($intKukaku == 1) {
        $intKukakuSquere = return_nm(max($aryKukakuSquere),'tubo');
    } elseif ($intKukaku == 0) {
        $intKukakuSquere = '--';
    } else {
        $intKukakuSquere = return_nm(min($aryKukakuSquere),'tubo').' ～ '.return_nm(max($aryKukakuSquere),'tubo');
    }
    $arytemp['kukaku'] = '区画面積：<br class="for-small">'.$intKukakuSquere;

?>
