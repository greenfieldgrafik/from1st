<?php
    /*

    # 投稿のカテゴリーを表示する

    ## 参考URL
    https://wpdocs.osdn.jp/%E3%83%86%E3%83%B3%E3%83%97%E3%83%AC%E3%83%BC%E3%83%88%E3%82%BF%E3%82%B0/get_the_category

    ## 使用方法

    include ./templates/php/get_category.php
    <?php echo $cat_name; ?>

    ### 注意事項
    ループ内で使用する
    出力する際は、<?php echo $cat_name; ?>など、echoで出力を行う

    */

    $category = get_the_category();
    // var_dump($category);
    $cat_id   = $category[0]->cat_ID;
    $cat_name = $category[0]->cat_name;
    $cat_slug = $category[0]->category_nicename;
?>
