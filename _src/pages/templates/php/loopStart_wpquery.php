<?php
    /*

    # 投稿を呼び出す

    ## 使用方法

    $target_post - 出力したい投稿タイプを指定する
    $post_count - 表示したい件数を指定する

    | <?php $target_post = 'post'; $post_count = '3'; ?>
    include ./templates/php/loopStart_wpquery.php
    ~ 記事の出力処理 ~
    include ./templates/php/loopEnd.php
    include ./templates/php/reset.php

    ### 注意事項
    loopEndとセットで使用すること

    */

    $paged = get_query_var('paged') ? get_query_var('paged') : 1;
    $args = array(
        'post_type' => array($target_post),
        'paged' => $paged,
        'cat' => $category,
        'post_status' => 'publish',
        'posts_per_page' => $post_count, // 表示するページ数
        'order' => 'DESC' // 並び順
    );

    $my_query = new WP_Query($args);

    if ($my_query->have_posts()) :
    while ( $my_query->have_posts() ) : $my_query->the_post();
?>
