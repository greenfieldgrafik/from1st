<?php
    /*

    # 投稿及びカスタム投稿の記事に指定したアイキャッチ画像を取得する

    ## $eye_imgのvar_dump情報

    array(4) { [0]=> string(xxx) "画像ファイルのURL" [1]=> int(画像の横幅) [2]=> int(画像の盾が場) [3]=> bool(false) }


    ## 使用方法

    include ./templates/php/eyecatch.php
    img(src!=`<?php echo $eye_img[0]; ?>`)

    ### 注意事項
    ループ内で使用すること

    */

    $thumb_id = get_post_thumbnail_id();
    $eye_img = wp_get_attachment_image_src( $thumb_id , 'full' );
?>
