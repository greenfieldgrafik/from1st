<?php
    /*

    # ターム（カスタム投稿タイプで作成したカテゴリー）を表示する

    ## 参考URL
    https://wpdocs.osdn.jp/%E9%96%A2%E6%95%B0%E3%83%AA%E3%83%95%E3%82%A1%E3%83%AC%E3%83%B3%E3%82%B9/get_the_terms

    ## 使用方法

    $taxonomyName - 呼び出したいタクソノミー名を指定する

    | <?php $taxonomyName = 'blog_cat'; ?>
    include ./templates/php/get_category_wpquery.php

    ### 注意事項
    ループ内で使用する
    出力する際は、echoで出力を行う
    例：<?php echo $cat_name; ?>

    */

    $terms = get_the_terms($post->ID,$taxonomyName);

    foreach($terms as $term1) :
        $cat_id = $term1->term_id;
        $cat_slug = $term1->slug;
        $cat_name = $term1->name;
        $cat_description = $term1->description;
    endforeach;
?>
