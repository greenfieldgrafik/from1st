<?php
    /*

    # ページネーションを作成する

    ## 参考URL
    https://wpdocs.osdn.jp/%E9%96%A2%E6%95%B0%E3%83%AA%E3%83%95%E3%82%A1%E3%83%AC%E3%83%B3%E3%82%B9/paginate_links

    ## 使用方法

    include ./templates/php/pageNav.php

    ### 注意事項
    アーカイブページで使用すること

    */

    $big = 999999999; // この記述が必要
    $end_size = 0 ;
    $args = array(
        'base'      => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),  // ベースURLの指定
        'current'   => max( 1, get_query_var('paged') ),  // 現在のページ番号
        'total'     => $wp_query->max_num_pages,  // 全体のページ数
        'end_size'  => $end_size,  // ページ番号のリストの両端（最初と最後）にいくつの数字を表示するか
        'mid_size'  => 2,  // 現在のページの両側にいくつの数字を表示するか ただし現在のページは含まない
        'prev_next' => true, // リストの中に「前へ」「次へ」のリンクを含むかどうか。
        'prev_text' => '', // 前のページへのリンクとして表示する文言
        'next_text' => '' // 次ページへのリンクとして表示する文言
    );
    echo paginate_links($args);
?>
