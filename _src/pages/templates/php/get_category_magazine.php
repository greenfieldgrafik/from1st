<?php
    // カスタムフィールドのカテゴリーを取得して情報を制御する
    $terms = get_the_terms($post->ID,'magazine_cat');

    foreach((array)$terms as $term1) :
        $currentCatSlug = $term1->slug;
        $currentCatTitle = $term1->name;
        $currentCatDescription = $term1->description;
        $currentCatId = $term1 -> term_id;
        $p_name_ja = SCF::get_term_meta( $currentCatId, 'magazine_cat', 'p_name_ja' );
        $p_name_en = SCF::get_term_meta( $currentCatId, 'magazine_cat', 'p_name_en' );
        $p_company = SCF::get_term_meta( $currentCatId, 'magazine_cat', 'p_company' );
        $p_portrate = SCF::get_term_meta( $currentCatId, 'magazine_cat', 'p_portrate' );
        $portrate_image = wp_get_attachment_image($p_portrate, 'full');
        $p_career = SCF::get_term_meta( $currentCatId, 'magazine_cat', 'p_career' );
        $p_about = SCF::get_term_meta( $currentCatId, 'magazine_cat', '会社概要' );
        $p_tel = SCF::get_term_meta( $currentCatId, 'magazine_cat', 'p_tel' );
        $p_site = SCF::get_term_meta( $currentCatId, 'magazine_cat', 'URL' );
        $p_awards = SCF::get_term_meta( $currentCatId, 'magazine_cat', 'p_awards' );
        $p_link = SCF::get_term_meta( $currentCatId, 'magazine_cat', 'p_link' );

    endforeach;

?>
