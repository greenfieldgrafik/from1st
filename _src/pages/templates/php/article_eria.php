<?php

    $aryEria = array();

    // wp-config
    global $glo_aryEria;

    // エリアでループ
    foreach ($glo_aryEria as $key => $value) {

        // タクソノミー内の記事一覧を取得
        $args = array(
            'numberposts' => -1,
            'post_type' => 'article',
            'meta_key' => 'area',
            'meta_value' => $value
        );

        global $post;
        $posts = get_posts($args);

        if($posts) {

            // 表示物件データ整形
            $aryArticle = array();

            foreach($posts as $post){

                $arytemp = array();
                $arytemp['image'] = get_field('image');
                $arytemp['image'] = wp_get_attachment_image_src($arytemp['image'],'medium');

                // 画像登録無し
                if ($arytemp['image'][0] == '') {
                    $arytemp['image'][0] = home_url('cms/wp-content/themes/official/assets/images/article/no-image.png');
                }

                $arytemp['name'] = get_field('name');
                $arytemp['state'] = get_field('state');
                $arytemp['address'] = get_field('address');
                $arytemp['link'] = get_permalink();

                $arytemp['class'] = '';
                switch($arytemp['state']) {
                    case '分譲中':
                        $arytemp['class'] = 'state1';
                        break;
                    case '造成途中':
                    case '近日造成':
                        $arytemp['class'] = 'state4';
                        break;
                    case '計画中':
                        $arytemp['class'] = 'state2';
                        break;
                    case '完売御礼':
                        $arytemp['class'] = 'state3';
                        break;
                }

                // ▽区画情報
                $aryKukaku = array();
                $aryKukaku = SCF::get('kukaku');

                // 区画数
                $intKukaku = 0;
                $intKukaku = count($aryKukaku);
                if ($intKukaku == 1) {
                    if ($aryKukaku[0]['k-no'] == '') {
                        $intKukaku = 0;
                    }
                }

                // 坪数の最大値、最小値を計測する為に配列に
                $intKukakuSquere = '';
                $aryKukakuSquere = array();
                foreach ($aryKukaku as $key_k => $value_k) {
                    $aryKukakuSquere[] = $value_k['k-square'];
                }

                if ($intKukaku == 1) {
                    $intKukakuSquere = return_nm(max($aryKukakuSquere),'tubo');
                } elseif ($intKukaku == 0) {
                    $intKukakuSquere = '--';
                } else {
                    $intKukakuSquere = return_nm(min($aryKukakuSquere),'tubo').' ～ '.return_nm(max($aryKukakuSquere),'tubo');
                }
                $arytemp['kukaku'] = '区画面積：<br class="for-small">'.$intKukakuSquere;

                array_push($aryArticle, $arytemp);
            }

            // エリア名をキーにした配列に格納
            $aryEria[$value] = $aryArticle;
        }

        // Query Reset
        wp_reset_query();
    }

?>
