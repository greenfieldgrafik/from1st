<?php
    /*

    # アーカイブページなどで投稿を呼び出す

    ## 使用方法

    include ./templates/php/loopStart.php
    ~ 記事の出力処理 ~
    include ./templates/php/loopEnd.php

    ### 注意事項
    アーカイブページで使用すること
    loopEndとセットで使用すること

    */
    if($posts) {
        foreach($posts as $post){

            if ( get_field('object') != '') {
                // 投稿オブジェクトを取得
                $object = get_field('object');
                $object_id = $object->ID;

                $arytemp = array();
                $arytemp['image'] = get_field('image', $object_id);
                $arytemp['image'] = wp_get_attachment_image_src($arytemp['image'],'thumbnail');

                // 画像登録無し
                if ($arytemp['image'][0] == '') {
                    $arytemp['image'][0] = home_url('cms/img/noimage.png');
                }

                $arytemp['link'] = get_permalink($object_id);

            } else {
                $arytemp['image'][0] = home_url('cms/img/noimage.png');
                $arytemp['link'] = '';
            }

            $arytemp['title'] = get_the_title();
            $arytemp['date'] = get_the_date();

            array_push($aryNews, $arytemp);
        }
    }
    wp_reset_query();   //Query Reset

    if (have_posts()) : while (have_posts()) : the_post();
?>
