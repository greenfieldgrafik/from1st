<?php
    /*

    # 日付ベースのアーカイブリストを表示する

    ## 参考URL
    https://wpdocs.osdn.jp/%E3%83%86%E3%83%B3%E3%83%97%E3%83%AC%E3%83%BC%E3%83%88%E3%82%BF%E3%82%B0/wp_get_archives

    ## 使用方法

    $postType - 投稿タイプを指定する
    $postArchive - 表示するアーカイブリストの種類を指定する

    | <?php $postType = 'post'; $postArchive = 'daily'; ?>
    include ./templates/php/get_archives.php

    ### 注意事項
    アーカイブページで使用すること

    */

    $args = array(
        'limit'     => '',            // 取得するアーカイブ数を制限する場合に使用する
        'post_type' => $postType,     // 投稿タイプを指定する
        'show_post_count' => false,   // 投稿数を表示するかの指定
        'type'      => $postArchive   // 表示するアーカイブリストの種類　yearly = 年, monthly = 月, daily = 日, weekly = 週
    );

    wp_get_archives( $args );
?>
