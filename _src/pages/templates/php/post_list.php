<?php
//********************************************************************
//* Title       フロムファースト　新着情報一覧表示テンプレート
//********************************************************************
    global $post;

    $strHtml = '';
    $cateid = '1';
    $aryNews = array();
    $posts = get_posts('numberposts=-1&category=' . $cateid);


    if($posts) {
        foreach($posts as $post){

            if ( get_field('object') != '') {
                // 投稿オブジェクトを取得
                $object = get_field('object');
                $object_id = $object->ID;

                $arytemp = array();
                $arytemp['image'] = get_field('image', $object_id);
                $arytemp['image'] = wp_get_attachment_image_src($arytemp['image'],'thumbnail');

                // 画像登録無し
                if ($arytemp['image'][0] == '') {
                    $arytemp['image'][0] = home_url('cms/img/noimage.png');
                }

                $arytemp['link'] = get_permalink($object_id);

            } else {
                $arytemp['image'][0] = home_url('cms/img/noimage.png');
                $arytemp['link'] = '';
            }

            $arytemp['title'] = get_the_title();
            $arytemp['date'] = get_the_date('Y.m.d');
            $arytemp['permission'] = SCF::get( 'n_permission' );
            $arytemp['release'] = SCF::get('n_release');

            array_push($aryNews, $arytemp);
        }
    }
    wp_reset_query();   //Query Reset



?>
