<?php
    $paged = get_query_var('paged') ? get_query_var('paged') : 1;
    $args = array(
        'post_type' => 'magazine',
        'paged' => $paged,
        'post_status' => 'publish',
        'posts_per_page' => $post_count, // 表示するページ数
        'order' => 'DESC' // 並び順
    );

    $my_query = new WP_Query($args);

    if ($my_query->have_posts()) :
    while ( $my_query->have_posts() ) : $my_query->the_post();
?>
