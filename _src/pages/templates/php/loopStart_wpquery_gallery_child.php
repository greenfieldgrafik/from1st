<?php

    // 中カテゴリのループ
    $childargs = array(
        'parent' => $parentId,
        'orderby' => 'term_order',
        'hide_empty' => false,
    );
    $childterms = get_terms($taxonomy, $childargs);

    foreach ((array)$childterms as $childterm) {
        // 中カテゴリのタームを取得
        $childterm = sanitize_term( $childterm, $taxonomy );
        // 中カテゴリのタクソノミーページへのURLを取得
        $childterm_link = get_term_link( $childterm, $taxonomy );
        // 中カテゴリのスラッグを取得
        $targetSlug = $childterm->slug;
        // タームのidを取得
        $term_scf = get_term_by( 'slug' , $targetSlug, $taxonomy );

        // 下の二つはスマートカスタムフィールド用
        $p_name_ja = SCF::get_term_meta( $term_scf->term_id, $taxonomy, 'p_name_ja' );
        $p_name_en = SCF::get_term_meta( $term_scf->term_id, $taxonomy, 'p_name_en' );
        $p_company = SCF::get_term_meta( $term_scf->term_id, $taxonomy, 'p_company' );
        $p_company_en = SCF::get_term_meta( $term_scf->term_id, $taxonomy, 'p_company_en' );
        $p_portrate = SCF::get_term_meta( $term_scf->term_id, $taxonomy, 'p_portrate' );
        $p_eyecatch = SCF::get_term_meta( $term_scf->term_id, $taxonomy, 'p_eyecatch' );
        $portrate_image = wp_get_attachment_image($p_portrate, 'large');
        $eyecatch_image = wp_get_attachment_image($p_eyecatch, 'large');
        $p_career = SCF::get_term_meta( $term_scf->term_id, $taxonomy, 'p_career' );
        $p_location = SCF::get_term_meta( $term_scf->term_id, $taxonomy, 'p_location' );
        $p_tel = SCF::get_term_meta( $term_scf->term_id, $taxonomy, 'p_tel' );
        $p_site = SCF::get_term_meta( $term_scf->term_id, $taxonomy, 'URL' );
        $p_awards = SCF::get_term_meta( $term_scf->term_id, $taxonomy, 'p_awards' );
        $p_link = SCF::get_term_meta( $term_scf->term_id, $taxonomy, 'p_link' );
        $p_movie = SCF::get_term_meta( $term_scf->term_id, $taxonomy, 'p_movie' );
        $detail_none = SCF::get_term_meta( $term_scf->term_id, $taxonomy, 'detail_none' );

?>
