# リポジトリの作成

~~~
mkdir /path/to/your/project
~~~
~~~
cd /path/to/your/project
~~~
~~~
`git init`
~~~

# プロジェクトの準備

1. すでにディレクトリがある場合は、リネーム
2. クローンしてデータを持ってくる
~~~
git clone https://????@bitbucket.org/greenfieldgrafik/zexy.git
~~~
3. 管理ディレクトリにリモート設定（通常は設定の必要なし）  
~~~
git remote add origin https://????@bitbucket.org/greenfieldgrafik/zexy.git
~~~
4. 管理ディレクトリにconfig設定  
~~~
git config user.name "名前"  
git config user.email "アカウント@greenfieldgrafik.com"
~~~

## configを確認する
~~~
git config --list
~~~

## パスワードを2回目以降に聞かれないようにするための設定 / osxkeychain
~~~
git config --global credential.helper osxkeychain
~~~


# Gulp Settings List

## Initial files
* `.bowerrc`
* `.editorconfig` EditorConfigを使用
* `.ftppass` FTPユーザー名とパスワードの格納先
* `.gitignore`
* `bower.json`
* `gulpfile.js`
* `package.json`
* `README.md`
* `settings.json` ファイル構造の変数や、Wordpress・FTPの情報を集約

## settings.json
* gulpfile.jsを編集しないことを基本思想とし、settings.jsonのみ編集する
* dirは基本編集しない
* Wordpressを利用する場合は`use: true`にする
* bowerfilesは使用するものを記載することでjs、cssにコピーされる
* sftpdemo、sftpdeployはコマンドラインからファイルをアップする際に利用する
  * ID、パスワードは別途.ftppassに記述する

## npm Command List
`gulp default`を実行
~~~
npm run build
~~~

`npm install`、`bower install`を実行
~~~
npm run setup
~~~

`gulp sftp:demo`を実行（デモ環境にファイルをアップ）
~~~
npm run demo
~~~

`gulp sftp:deploy`を実行（本番環境にファイルをアップ）
~~~
npm run deploy
~~~

`gulp watch`を実行
~~~
npm run watch
~~~


## Gulp Install Module List

* `browser-sync` ブラウザでの表示確認
* `del` 指定したファイルの削除
* `gulp`
* `gulp-autoprefixer` ベンダープレフィックスを自動で付与
* `gulp-concat` 複数のファイルを一つのファイルに結合
* `gulp-favicons` 指定した画像を元にfaviconを生成
* `gulp-imagemin` 画像を最適化（圧縮）
* `gulp-if` if文を使用可能にする
* `gulp-jade` jadeを使用
* `gulp-jshint` jsが正しく記述できてるかを検証する
* `gulp-load-plugins` gulp-で始まるモジュールを自動的に読み込み
* `gulp-newer` ファイルの日付を見て新しければ処理を行う
* `gulp-notify` エラー発生時に通知を表示
* `gulp-plumber` 処理中にエラーが発生しても、処理を止めない
* `gulp-rename` ファイルのリネームや拡張子の変更
* `gulp-sass` sassを使用
* `gulp-sftp` CLIからファイルのアップロード
* `gulp-uglify` JSのminify
* `jshint` gulp-jshintインストール時に合わせてインストール
* `run-sequence` タスクの直列処理（gulp v4以上になると不要）


## その他・備考

### Node Globについて
gulpfile.js内の一部記述（dot:trueやnodir:trueなど）はこちらを参照
https://github.com/isaacs/node-glob

### npm installの速度を改善する
プログレスバーの処理をなくすことでインストール速度が改善される
http://peccu.hatenablog.com/entry/2016/02/03/000000
~~~
$ npm set progress=false
~~~

### macaroni + greenfieldgrafik

#### JSインストールファイルについて

* _intro.js
* accordion.js
* addClassEq.js
* addClassSc.js
* beltline.js
* gmap.js
* imageTicker.js
* lib
  * checkBrowser.js
  * checkBrowserSc.js
  * develop.js
* mainVisual.js
* multiNav.js
* pageToggleNav.js
* toggleNav.js
* z_outro.js

#### imagesファイルについて

* beltline

#### wpjadeファイルについて

* assets
  * functions.php
  * screenshot.jpg
  * style.css
* index.jade
* templates
  * _favicon.jade
  * _fb.jade
  * _ga.jade
  * _layout.jade
  * php
    * eyecatch.php
    * get_category_categoryname.php
    * get_category.php
    * get_catlist_categoryname.php
    * get_catlist.php
    * get_customfield_photo.php
    * get_customfield_term_end.php
    * get_customfield_term_start.php
    * get_customfiled_var_categoryname.php
    * get_post_image.php
    * get_taglist.php
    * loopEnd.php
    * loopEndWhile.php
    * loopStart_post_categoryname.php
    * loopStart_tag.php
    * loopStart_tax_categoryname.php
    * loopStart_wpquery.php
    * loopStart.php
    * pageNav_wpquery.php
    * pageNav.php
    * reset.php

#### WordPress Plugins

* Custom Post Type Permalinks
* Duplicate Post
* Google XML Sitemaps
* MW WP Form
* Resize Image After Upload
* Show Current Template
* Smart Custom Fields
* WP Multibyte Patch
